#pragma once

#include"EasyXGameEngine.h"
#include<time.h>
#include<stdlib.h>
#include<math.h>

//游戏相关变量的定义

#define HIGH 400
#define WIDTH 560

#define NUMENMY 20
#define NUMHOSTAGE 6

bool g_IsReStart=false;

char g_CurrentDir[260];
char g_Weapongun[10] = "武器：GND";
char g_WeaponDD[10] = "武器：DD";

RECT g_PlayerBlood;
RECT g_PlayerBloodSolid;

RECT g_rePlaneBlood;
RECT g_rePlaneBloodSolid;

EasyxGameEngine g_GameEngine;
HWND g_hWnd;//用来储存的窗口句柄

bool isJump;//用于来存储是否跳起
int g_GJump;

DWORD threadID;//线程号,创建线程播放音乐，解决卡顿
//HANDLE handthread = ::CreateThread(0, 0, Recordthis, currentdir, 0, &threadID);

long g_startbuttondown;//给按下一个键后动画提供一定的执行时间
int g_buttondowmdelay;
bool g_buttondown;//是否有需要这个延迟的动画；

long g_starttime_button;//为按键提供延迟,防止一个键在极短时间内，连续按下
int g_button_delay;

//根据枚举变量播放音乐
enum Musicflg{ BKMUSIC1 = 0,PICK,BKMUSIC2,BULLETMUSIC,PLAYERDEAD
	,MISSIONCOMPLETE,ENMYDEAD, DEFAULT,GUNSHOOT , SHOULEI,XIU,PLANEBOMB,CLOSE0,ALLSTART0, ALLSTART1,CLOSE1};
enum PeopleState{LIFE,DEAD};
enum Weapon{GUN,DD};

Weapon Weapons;

int g_scene;//场景编号
Scene Scene0;//第一个场景即开始场景
Scene Scene1;//正式的游戏场景
Scene Scene2;//0子场景


RECT g_Statement;
RECT g_Return;
RECT g_StartButton;

int g_nDiraction;//1为右方，2为左方

//.....复杂的人物动画4。。。。shit
Sprite SpritePlayerDownL, SpritePlayerDownR;
Sprite SpritePlayerUpLUpNorlmal, SpritePlayerUpLHorNormal;//玩家的动画
Sprite SpritePlayerUpRUpNorlmal, SpritePlayerUpRHorNormal;//玩家的动画
Sprite SpritePlayerUpLUpgun, SpritePlayerUpLHorgun;//玩家的动画
Sprite SpritePlayerUpRUpgun, SpritePlayerUpRHorgun;//玩家的动画
Sprite SpritePlayerRunR, SpritePlayerRunL;

Sprite SpritePlayerShoulei;
Sprite SpritePlayerShouleiL;
Sprite SpriteShoulei[10];
Sprite SpriteShouleiL[10];
Sprite SpriteShouleiExplode[10];
int g_ShouleiNum;
int g_ShouleiNumL;
int g_ShouleiExplodeNum;
int g_ShouleiInternal;//用于设置间隔，防止一直按下不断弹出手雷
long g_ShouleiStarttime;
int g_ShouleiInternalL;
long g_ShouleiStarttimeL;

//子弹的动画
int g_NumBullte;
int g_NumBullteL;
int g_NumBullteU;
Sprite SpriteGunBullte[50];
Sprite SpriteGunBullteL[50];
Sprite SpriteGunBullteU[50];
Sprite SpriteDDBullte[50];
Sprite SpriteDDBullteL[50];
Sprite SpriteDDBullteU[50];
Sprite SpriteDDBullteW[50];
Sprite SpriteDDBullteWL[50];
Sprite SpriteDDBullteWU[50];
Sprite SpriteDDExplode[50];

Sprite SpriteHostage[NUMHOSTAGE];//人质的动画（坐）
Sprite SpriteHostageRescue[NUMHOSTAGE];//人质的动画（裂绳子）
Sprite SpriteHostageRun[NUMHOSTAGE];//人质的动画（奔跑）
Sprite SpriteHostageGive[NUMHOSTAGE];//掀衣服的动画（笑）
Sprite SpriteHostageGift[NUMHOSTAGE];//水果的动画

Sprite SpriteOilJar[NUMHOSTAGE];//油罐
int g_GunShootOil[NUMHOSTAGE];//油罐被手枪命中的次数
Sprite SpriteOilJarExplode[NUMHOSTAGE];//油罐爆炸
Sprite SpriteOilJarScrap[NUMHOSTAGE];//报废的油罐

long g_socre;//计分
Sprite SpriteSocre0[13];//分数的精灵
Sprite SpriteSocre1[13];
Sprite SpriteSocre2[13];
Sprite SpriteSocre3[13];
Sprite SpriteSocre4[13];
Sprite SpriteSocre5[13];
Sprite SpriteSocre6[13];
Sprite SpriteSocre7[13];
Sprite SpriteSocre8[13];
Sprite SpriteSocre9[13];


int g_bulletinteral;
long g_bulletstarttime;//朝右的子弹

int g_bulletinteralL;
long g_bulletstarttimeL;//朝左的

int g_bulletinteralU;//朝上的
long g_bulletstarttimeU;

Sprite g_Pang[3];//场景1中的标签
Sprite g_MilltaryRescue;
Sprite g_Fisrt;
Sprite g_MissionStart;//场景2中的标签
RECT g_reStart;//场景1中的开始和退出按钮
RECT g_reEnd;
RECT g_store;

Sprite SpritePlane;
Sprite SpritePlaneBomb[50];
Sprite SpritePlaneBombExplode[50];
int g_PlaneBlood;
int g_PlaneBombNum;
int g_PlaneBomDelay;
long g_PlaneBombStarttime;


Sprite SpriteEnmy[NUMENMY];//敌人的动画
Sprite SpriteEnmyL[NUMENMY];//敌人的动画左边
Sprite SpriteEnmyP[NUMENMY];//拿大炮的敌人
Sprite SpriteEnmyPL[NUMENMY];

PeopleState EnmySate[NUMENMY];//关联着每一个
PeopleState EnmyPSate[NUMENMY];

int g_EnmydeadNum;
int g_EnmydeadNumL;
Sprite SpriteEnmyDead[NUMENMY];
Sprite SpriteEnmyDeadL[NUMENMY];
Sprite SpriteEnmyDeadP[NUMENMY];
Sprite SpriteEnmyDeadPL[NUMENMY];

int g_EnmybulletNum;
int g_EnmybulletNumP;
int g_EnmybulletNumL;
int g_EnmybulletNumPL;
Sprite SpriteEnmyBullet[50];
Sprite SpriteEnmyBulletP[50];
Sprite SpriteEnmyBulletL[50];
Sprite SpriteEnmyBulletPL[50];

void Scene0Action();//场景1的逻辑
void Scene1Action();//场景2的逻辑
void Scene0Collision();//场景1的碰撞检测
void Scene1Collision();//场景2的碰撞检测
void Scene0KeyEvent();//场景1的按键处理
void Scene1KeyEvent();//场景2的按键处理
void Scene0MouseEvent(MOUSEMSG msg);//场景1的鼠标消息处理
void Scene1MouseEvent(MOUSEMSG msg);//场景2的鼠标消息处理
void CloseScene0Music();//关闭场景1音乐

void CloseScene1Music();//关闭场景2音乐
void CloseScene0();//关闭场景1
void CloseScene1();//关闭场景2
void Scene0Start();//初始化场景1
void Scene1Start();//初始化场景2

DWORD WINAPI Scene0Music(void* flg);//场景1音乐
DWORD WINAPI Scene1Music(void* flg);//场景2音乐

Musicflg Musicflgs= Musicflg::DEFAULT;

void Scene2Start();
void CloseScene2();
void Scene2MouseEvent(MOUSEMSG msg);


//封装人物动作的函数
void PlayerForward();//人物向前移动
void PlayerBack();
void PlayerJump(int nDirection);
void PlayerShoot(int nDirection);
void PlayerDead(int nDirection);
void PlayerDefalut(int nDirection);
void PlayerUp();
void PlayerUpShoot();
void PlayerShoulei();
void ThrowShoulei();
void ShouleiBoom(int num,int direction);//爆炸指定的手雷
void ShowSocre(long socre);//显示分数，//分数的具体实现就是在所有位置都有10个数，通过显示隐藏来控制分数的显示
void DDExplode(int num,int x,int y);//导弹爆炸
void PlaneBombExplode(int num);//导弹爆炸
void EnmyDead(int nDirection,int num,bool isp);
void EnmyGunShoot(int nDiraction,bool isp,int x,int y);