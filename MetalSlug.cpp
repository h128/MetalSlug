#include"MetalSlug.h"

bool GameStart()//游戏初始化
{
	bool m_Init = true;
	g_scene = 0;//初始化场景编号,其实引擎也内置为0
	g_nDiraction = 1;//初始化方向
	g_hWnd=initgraph(WIDTH, HIGH);//建立窗口
	//以下为场景的基本初始化
	Scene0.Init(0,0,2,CloseScene0,Scene0Start,Scene0Music,CloseScene0Music,Scene0Action,Scene0KeyEvent,
		Scene0MouseEvent,Scene0Collision);
	Scene1.Init(0, 0, 2, CloseScene1, Scene1Start, Scene1Music, CloseScene1Music, Scene1Action, Scene1KeyEvent,
		Scene1MouseEvent, Scene1Collision);
	Scene2.Init(0, 0, 1, CloseScene2, Scene2Start, NULL, NULL, NULL, NULL,
		Scene2MouseEvent, NULL);
	//引擎的初始化
	g_GameEngine.Init(18, timeGetTime());
	g_GameEngine.AddScene(&Scene0);
	g_GameEngine.AddScene(&Scene1);
	g_GameEngine.AddScene(&Scene2);
	//执行当前的场景初始化函数
	g_GameEngine.GetCurrentScene()->m_scenestart();
	//在这里开一个线程用于播放音乐
	HANDLE handthread = CreateThread(0, 0,GameMusic, 0, 0, &threadID);
	return m_Init;
}
void GamePaint()//游戏画面绘制
{
	BeginBatchDraw();//批量绘制
	g_GameEngine.GetCurrentScene()->DrawAll();//调用内置的绘图函数
	if (g_GameEngine.m_sceneID == 1)
	{
		settextstyle(20, 15, "黑体");
		if (Weapons == Weapon::DD)
			outtextxy(0, SpriteSocre0->images.at(0)->image->getheight()+13, g_WeaponDD);
		else if (Weapons == Weapon::GUN)
			outtextxy(0, SpriteSocre0->images.at(0)->image->getheight()+13, g_Weapongun);
		setfillcolor(RGB(0, 255, 0));
		setcolor(RGB(255, 255, 255));
		rectangle(g_PlayerBlood.left, g_PlayerBlood.top, g_PlayerBlood.right, g_PlayerBlood.bottom);
		solidrectangle(g_PlayerBloodSolid.left, g_PlayerBloodSolid.top, g_PlayerBloodSolid.right, g_PlayerBloodSolid.bottom);

		if (-Scene1.m_bkx >= (WIDTH*(Scene1.m_bkimages.size() - 1)))//人物移动到最后一个场景
		{
			float bl=g_PlaneBlood / 20000.0;
			g_rePlaneBloodSolid.right = g_rePlaneBloodSolid.left + (WIDTH-260)*bl;
			setfillcolor(RGB(255, 0, 0));
			rectangle(g_rePlaneBlood.left, g_rePlaneBlood.top, g_rePlaneBlood.right, g_rePlaneBlood.bottom);
			solidrectangle(g_rePlaneBloodSolid.left, g_rePlaneBloodSolid.top, g_rePlaneBloodSolid.right, g_rePlaneBloodSolid.bottom);
		}

		if (g_MissionStart.m_visible == true)
		{
			FlushBatchDraw();
			Sleep(1500);
			g_MissionStart.m_visible = false;
		}
	}
	FlushBatchDraw();
	EndBatchDraw();
}
void GameAction()//游戏逻辑
{
	if(g_GameEngine.GetCurrentScene()->m_sceneaction!=NULL)
		g_GameEngine.GetCurrentScene()->m_sceneaction();
	GameCollision();//进行碰撞检测
	GamePaint();//画面渲染
}
bool GameCollision()//游戏碰撞检测
{
	if(g_GameEngine.GetCurrentScene()->m_scenecollision!=NULL)
		g_GameEngine.GetCurrentScene()->m_scenecollision();
	return true;
}
void GameEnd()//游戏结束
{
	closegraph();
}

DWORD WINAPI GameMusic(void* nflg)//游戏音乐
{
	/*if (g_GameEngine.GetCurrentScene()->m_scenemusic != NULL)
	{
		HANDLE handthread = CreateThread(0, 0, g_GameEngine.GetCurrentScene()->m_scenemusic, nflg, 0, &threadID);
	}*/
		//g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);
	while (true)
	{
		switch (Musicflgs)
		{
		case Musicflg::ALLSTART0:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("open resources\\music\\bkstart.mp3 alias bkstart", NULL, 0, NULL);
			mciSendString("play bkstart repeat", NULL, 0, NULL);
			break;
		case Musicflg::CLOSE0:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("close bkstart", NULL, 0, NULL);
			break;
		case Musicflg::ALLSTART1:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("open resources\\music\\bkmain.mp3 alias bkmain", NULL, 0, NULL);
			mciSendString("open resources\\music\\Mission1Start.wav alias mission1start", NULL, 0, NULL);
			mciSendString("play bkmain repeat", NULL, 0, NULL);
			mciSendString("play mission1start", NULL, 0, NULL);
			break;
		case Musicflg::CLOSE1:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("close bkmain", NULL, 0, NULL);
			mciSendString("close missioncomplete", NULL, 0, NULL);
			break;
		case Musicflg::MISSIONCOMPLETE:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("open resources\\music\\MissionComplete.wav alias missioncomplete", NULL, 0, NULL);
			mciSendString("play missioncomplete", NULL, 0, NULL);
			break;
		case Musicflg::SHOULEI:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("close shouleiboom", NULL, 0, NULL);
			mciSendString("open resources\\music\\shoulei.mp3 alias shouleiboom", NULL, 0, NULL);
			mciSendString("play shouleiboom", NULL, 0, NULL);
			break;
		case Musicflg::PLANEBOMB:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("close planebomb", NULL, 0, NULL);
			mciSendString("open resources\\music\\jetExplosion.mp3 alias planebomb", NULL, 0, NULL);
			mciSendString("play planebomb", NULL, 0, NULL);
			break;
		case Musicflg::PICK:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("close pick", NULL, 0, NULL);
			mciSendString("open resources\\music\\pick.mp3 alias pick", NULL, 0, NULL);
			mciSendString("play pick", NULL, 0, NULL);
			break;
		case Musicflg::XIU:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("close xiu", NULL, 0, NULL);
			mciSendString("open resources\\music\\bombFall.mp3 alias xiu", NULL, 0, NULL);
			mciSendString("play xiu", NULL, 0, NULL);
			break;
		case Musicflg::ENMYDEAD:
			Musicflgs = Musicflg::DEFAULT;
			mciSendString("close enmydead", NULL, 0, NULL);
			mciSendString("open resources\\music\\enemyDie.mp3 alias enmydead", NULL, 0, NULL);
			mciSendString("play enmydead", NULL, 0, NULL);
			break;
		case Musicflg::DEFAULT:
			break;
		}
	}
	return true;
}

void KeyEvent()//按键处理
{
	if(g_GameEngine.GetCurrentScene()->m_scenekeyevent!=NULL)
		g_GameEngine.GetCurrentScene()->m_scenekeyevent();
}
void MouseEvent(MOUSEMSG msg)//鼠标处理
{
	if(g_GameEngine.GetCurrentScene()->m_scenemouseevent!=NULL)
		g_GameEngine.GetCurrentScene()->m_scenemouseevent(msg);
}


void Scene0Action()//场景1的逻辑
{
	
}
void Scene1Action()//场景2的逻辑
{
	//用来管理跳起,简化的
	if (isJump)
	{
		SpritePlayerDownL.reduceXY(0, g_GJump);
		SpritePlayerDownR.reduceXY(0, g_GJump);
		SpritePlayerUpLUpNorlmal.reduceXY(0, g_GJump); 
		SpritePlayerUpLHorNormal.reduceXY(0, g_GJump);//玩家的动画
		SpritePlayerUpRUpNorlmal.reduceXY(0, g_GJump); 
		SpritePlayerUpRHorNormal.reduceXY(0, g_GJump);//玩家的动画
		SpritePlayerUpLUpgun.reduceXY(0, g_GJump);
		SpritePlayerUpLHorgun.reduceXY(0, g_GJump);//玩家的动画
		SpritePlayerUpRUpgun.reduceXY(0, g_GJump); 
		SpritePlayerUpRHorgun.reduceXY(0, g_GJump);//玩家的动画
		SpritePlayerRunR.reduceXY(0, g_GJump);
		SpritePlayerRunL.reduceXY(0, g_GJump); 
		SpritePlayerShoulei.reduceXY(0, g_GJump);
		SpritePlayerShouleiL.reduceXY(0, g_GJump);
		g_GJump -= 1;
		if (g_GJump <= -19)
			isJump = false;
	}


	if (g_PlayerBloodSolid.right <= 0)
	{
		
		MessageBox(g_hWnd, "你死了\n结束游戏!", "提示", MB_OK | MB_ICONERROR);
		exit(0);
		/*{
			//CloseScene1();
			//GameStart();
			CloseScene1Music();
			g_GameEngine.m_sceneID = 0;
			g_GameEngine.GetCurrentScene()->m_scenestart();
			g_IsReStart = true;
		}
		else
		{
			exit(0);
		}*/
	}
	if (g_PlaneBlood <= 0)
	{
		Musicflgs = Musicflg::MISSIONCOMPLETE;
		if (SpritePlane.m_y[0] >= 275)
		{
			MessageBox(g_hWnd, "恭喜你！！！！\n你赢得了游戏\n结束游戏", "提示", MB_OK | MB_ICONINFORMATION);
			exit(0);
		}

		/*{
			//CloseScene1();
			CloseScene1Music();
			g_GameEngine.m_sceneID = 0;
			g_GameEngine.GetCurrentScene()->m_scenestart();
			g_IsReStart = true;
			//GameStart();
		}
		else
		{
			exit(0);
		}*/
	}
	if (g_PlaneBlood > 0)
	{
		//这里用来管理最后的大boss
		if (-Scene1.m_bkx >= (WIDTH*(Scene1.m_bkimages.size() - 1)))//人物移动到最后一个场景
		{
			//这两行用来让飞机向人物移动
			if (SpritePlane.m_x[0] > SpritePlayerDownR.m_x[0] - 30)
			{
				SpritePlane.m_x[0] -= 2;
				for (int i = 0; i < 50; i++)
				{
					if (SpritePlaneBomb[i].m_visible == false)
						SpritePlaneBomb[i].m_x[0] -= 2;
				}
			}
			if (SpritePlane.m_x[0] < SpritePlayerDownR.m_x[0] - 30)
			{
				SpritePlane.m_x[0] += 2;//飞机的移动
				for (int i = 0; i < 50; i++)
				{
					if (SpritePlaneBomb[i].m_visible == false)
						SpritePlaneBomb[i].m_x[0] += 2;//子弹随飞机移动
				}
			}
			if (SpritePlane.m_x[0]<SpritePlayerDownR.m_x[0] - 10 && SpritePlane.m_x[0]>SpritePlayerDownR.m_x[0] - 50)
			{
				if (timeGetTime() - g_PlaneBombStarttime >= g_PlaneBomDelay)
				{
					//到了一定时间间隔就显示炸弹
					/*int* pflg = new int;
					*pflg = Musicflg::XIU;
					GameMusic(pflg);*/
					Musicflgs = Musicflg::XIU;
					SpritePlaneBomb[g_PlaneBombNum].m_visible = true;
					g_PlaneBombNum++;
					g_PlaneBombStarttime = timeGetTime();
				}
			}
		}
	}
	else
	{
		if(SpritePlane.m_y[0]<=275)
			SpritePlane.m_y[0] += 4;
	}
	

	if (g_NumBullte == 50)//用于控制多个子弹，和手雷，其实有点累赘
		g_NumBullte = 1;
	if (g_NumBullteL == 50)
		g_NumBullteL = 1;
	if (g_NumBullteU == 50)
		g_NumBullteU = 1;
	if (g_ShouleiNum == 10)
		g_ShouleiNum = 1;
	if (g_ShouleiNumL == 10)
		g_ShouleiNumL = 1;
	if (g_ShouleiExplodeNum == 10)
		g_ShouleiExplodeNum = 1;
	if (g_PlaneBombNum == 50)
		g_PlaneBombNum = 1;
	if (g_EnmydeadNum == NUMENMY)
		g_EnmydeadNum = 1;
	if (g_EnmydeadNumL == NUMENMY)
		g_EnmydeadNumL = 1;
	if (g_EnmybulletNum == 50)
		g_EnmybulletNum = 1;
	if (g_EnmybulletNumP == 50)
		g_EnmybulletNumP = 1;
	if (g_EnmybulletNumL == 50)
		g_EnmybulletNumL = 1;
	if (g_EnmybulletNumPL == 50)
		g_EnmybulletNumPL = 1;
	//SpritePlayerDownR.m_x += 5;

	for (int i = 1; i < 50; i++)
	{

		if (SpriteEnmyBullet[i].m_visible)
			SpriteEnmyBullet[i].m_x[0] += 2;
		if (SpriteEnmyBulletP[i].m_visible)
			SpriteEnmyBulletP[i].m_x[0] += 2;
		if (SpriteEnmyBulletL[i].m_visible)
			SpriteEnmyBulletL[i].m_x[0] -= 2;
		if (SpriteEnmyBulletPL[i].m_visible)
			SpriteEnmyBulletPL[i].m_x[0] -= 2;

		/*if (Weapons == Weapon::GUN)
		{*/
			if (SpriteGunBullte[i].m_visible == true)//&& SpriteGunBullte[i].m_tempindex== SpriteGunBullte[i].m_index-1)
				SpriteGunBullte[i].addXY(18, 0);
			if (SpriteGunBullteL[i].m_visible == true)//&& SpriteGunBullteL[i].m_tempindex == SpriteGunBullteL[i].m_index-1)
				SpriteGunBullteL[i].addXY(-18, 0);//移动子弹的最后的帧，即子弹的速度
			if (SpriteGunBullteU[i].m_visible == true)//&& SpriteGunBullteU[i].m_tempindex == SpriteGunBullteU[i].m_index-1)
				SpriteGunBullteU[i].addXY(0, -18);//移动子弹的最后的帧，即子弹的速度
		/*}
		else if (Weapons == Weapon::DD)
		{*/
			if (SpriteDDBullte[i].m_visible == true)
				SpriteDDBullte[i].addXY(7, 0);
			if (SpriteDDBullteL[i].m_visible == true)
				SpriteDDBullteL[i].addXY(-7, 0);//移动导弹的最后的帧，即子弹的速度
			if (SpriteDDBullteU[i].m_visible == true)
				SpriteDDBullteU[i].addXY(0, -7);//移动导弹的最后的帧，即子弹的速度
			if (SpriteDDBullteW[i].m_visible == true)
				SpriteDDBullteW[i].addXY(7, 0);
			if (SpriteDDBullteWL[i].m_visible == true)
				SpriteDDBullteWL[i].addXY(-7, 0);//移动导弹的最后的帧，即子弹的速度
			if (SpriteDDBullteWU[i].m_visible == true)
				SpriteDDBullteWU[i].addXY(0, -7);//移动导弹的最后的帧，即子弹的速度
		//}

			//如果飞机炸弹显示出来了，就下落
			if (SpritePlaneBomb[i].m_visible)
			{
				if(SpritePlaneBomb[i].m_y[0]<300)
					SpritePlaneBomb[i].m_y[0] += 3;
				else//如果显示出来了飞机炸弹且落到了相应的位置，就爆炸重置
				{
					PlaneBombExplode(i);
					if (SpriteInSprite(&SpritePlaneBombExplode[i], &SpritePlayerDownL))
					{
						g_PlayerBloodSolid.right -= 10;
					}
					if (SpriteInSprite(&SpritePlaneBombExplode[i], &SpritePlayerDownR))
					{
						g_PlayerBloodSolid.right -= 10;
					}
					SpritePlaneBomb[i] = SpritePlaneBomb[0];
					
				}
			}

			//这里用来设计敌人的AI(很简单).借用循环，提高游戏运行的效率
			if (i - 1 < NUMENMY)
			{
				if (EnmySate[i - 1] == PeopleState::LIFE)
				{
					if (SpriteEnmy[i - 1].m_x[0] > SpritePlayerDownR.m_x[0] + 50)//先判断敌人和玩家的位置
					{
						//先将另一个方向的隐藏起来,打开另一方
						SpriteEnmy[i - 1].m_visible = false;
						SpriteEnmyL[i - 1].m_visible = true;
						//敌人在玩家右边
						if (SpriteEnmyL[i - 1].m_x[0] >= -100 && SpriteEnmyL[i - 1].m_x[0] <= WIDTH + 100)//如果敌人在屏幕内
						{
							//设置敌人的移动
							if (SpriteEnmyL[i - 1].m_mode == SpriteMode::ONCEL&&SpriteEnmyL[i - 1].m_pausedelay == 0)
							{
								SpriteEnmyL[i - 1].m_x[0] -= 2;
								SpriteEnmy[i - 1].m_x[0] -= 2;
							}
							if (SpriteEnmyL[i - 1].m_tempindex == SpriteEnmyL[i - 1].m_index - 1 && SpriteEnmyL[i - 1].m_pausedelay == 0)//人物播放到最后
							{
								//发射子弹
								EnmyGunShoot(2,false, SpriteEnmyL[i - 1].m_x[0], SpriteEnmyL[i - 1].m_y[0]+10);
								//重新跑动
								//SpriteEnmyL[i - 1].m_tempindex = 0;//重新设置索引,让下面代码来重设模式
								SpriteEnmyL[i - 1].addPauseDelay(1000);
								SpriteEnmyL[i - 1].m_mode = SpriteMode::ONCEL;
							}
							
							//设置敌人的播放形式
							if (SpriteEnmyL[i - 1].m_tempindex != SpriteEnmyL[i - 1].m_index - 1)//就是人物没有播放到最后
								SpriteEnmyL[i - 1].m_mode = SpriteMode::ONCEL;
						}


					}
					else if (SpriteEnmyL[i - 1].m_x[0] < SpritePlayerDownR.m_x[0] - 50)//敌人在玩家的左边
					{
						//先将另一个方向的隐藏起来,打开另一方
						SpriteEnmyL[i - 1].m_visible = false;
						SpriteEnmy[i - 1].m_visible = true;
						if (SpriteEnmy[i - 1].m_x[0] >= -100 && SpriteEnmy[i - 1].m_x[0] <= WIDTH + 100)//如果敌人在屏幕内
						{
							if (SpriteEnmy[i - 1].m_mode == SpriteMode::ONCEL&&SpriteEnmy[i - 1].m_pausedelay == 0)
							{
								SpriteEnmyL[i - 1].m_x[0] += 2;
								SpriteEnmy[i - 1].m_x[0] += 2;
							}
							if (SpriteEnmy[i - 1].m_tempindex == SpriteEnmy[i - 1].m_index - 1 && SpriteEnmy[i - 1].m_pausedelay == 0)//就是人物没有播放到最后
							{
								//发射子弹
								EnmyGunShoot(1,false,SpriteEnmy[i - 1].m_x[0] + 30, SpriteEnmy[i - 1].m_y[0] + 10);
								//重新跑动
								//SpriteEnmy[i - 1].m_tempindex = 0;
								SpriteEnmy[i - 1].addPauseDelay(1000);
								SpriteEnmy[i - 1].m_mode = SpriteMode::ONCEL;
							}
							//设置敌人的播放形式
							if (SpriteEnmy[i - 1].m_tempindex != SpriteEnmy[i - 1].m_index - 1)//就是人物没有播放到最后
								SpriteEnmy[i - 1].m_mode = SpriteMode::ONCEL;
						}
					}
				}
				
				if (EnmyPSate[i - 1] == PeopleState::LIFE)
				{//同上这是对炮兵的处理
					if (SpriteEnmyP[i - 1].m_x[0] > SpritePlayerDownR.m_x[0] + 50)//先判断敌人和玩家的位置
					{
						SpriteEnmyP[i - 1].m_visible = false;
						SpriteEnmyPL[i - 1].m_visible = true;

						if (SpriteEnmyPL[i - 1].m_x[0] >= -100 && SpriteEnmyPL[i - 1].m_x[0] <= WIDTH + 100)//如果敌人在屏幕内
						{
							if (SpriteEnmyPL[i - 1].m_mode == SpriteMode::ONCEL&&SpriteEnmyPL[i - 1].m_pausedelay == 0)
							{
								SpriteEnmyP[i - 1].m_x[0] -= 2;
								SpriteEnmyPL[i - 1].m_x[0] -= 2;
							}
							if (SpriteEnmyPL[i - 1].m_tempindex == SpriteEnmyPL[i - 1].m_index - 1 && SpriteEnmyPL[i - 1].m_pausedelay == 0)
							{
								//发射子弹
								EnmyGunShoot(2, true, SpriteEnmyPL[i - 1].m_x[0], SpriteEnmyPL[i - 1].m_y[0] + 10);
								//重新跑动
								//SpriteEnmyPL[i - 1].m_tempindex = 0;
								SpriteEnmyPL[i - 1].addPauseDelay(1000);
								SpriteEnmyPL[i - 1].m_mode = SpriteMode::ONCEL;
							}
							//设置敌人的播放形式
							if (SpriteEnmyPL[i - 1].m_tempindex != SpriteEnmyPL[i - 1].m_index - 1)//就是人物没有播放到最后
								SpriteEnmyPL[i - 1].m_mode = SpriteMode::ONCEL;
						}
					}
					else if (SpriteEnmyPL[i - 1].m_x[0] < SpritePlayerDownR.m_x[0] - 50)
					{
						SpriteEnmyPL[i - 1].m_visible = false;
						SpriteEnmyP[i - 1].m_visible = true;

						if (SpriteEnmyP[i - 1].m_x[0] >= -100 && SpriteEnmyP[i - 1].m_x[0] <= WIDTH + 100)//如果敌人在屏幕内
						{
							if (SpriteEnmyP[i - 1].m_mode == SpriteMode::ONCEL&&SpriteEnmyP[i - 1].m_pausedelay == 0)
							{
								SpriteEnmyP[i - 1].m_x[0] += 2;
								SpriteEnmyPL[i - 1].m_x[0] += 2;
							}
							if (SpriteEnmyP[i - 1].m_tempindex == SpriteEnmyP[i - 1].m_index - 1 && SpriteEnmyP[i - 1].m_pausedelay == 0)
							{
								//发射子弹
								EnmyGunShoot(1, true, SpriteEnmyP[i - 1].m_x[0] + 30, SpriteEnmyP[i - 1].m_y[0] + 10);
								//SpriteEnmyP[i - 1].m_tempindex = 0;
								SpriteEnmyP[i - 1].addPauseDelay(1000);
								SpriteEnmyP[i - 1].m_mode = SpriteMode::ONCEL;
							}
							//设置敌人的播放形式
							if (SpriteEnmyP[i - 1].m_tempindex != SpriteEnmyP[i - 1].m_index - 1)//就是人物没有播放到最后
								SpriteEnmyP[i - 1].m_mode = SpriteMode::ONCEL;
						}
					}
				}
				
			}
			
	}
	for (int i = 0; i < NUMHOSTAGE; i++)//人质逃跑
	{
		if (SpriteHostageRun[i].m_visible&&SpriteHostageRun[i].m_playdelay == 0)
		{
			SpriteHostageRun[i].addXY(-3, 0);//人质逃跑的移动
		}
		if (SpriteHostageGift[i].m_visible&&SpriteHostageGift[i].m_playdelay == 0)
		{
			if(SpriteHostageGift[i].m_y[0]<=305)//确定水果落下的位置
				SpriteHostageGift[i].addXY(0, 3);
		}
	}
	
	ShowSocre(g_socre);//显示分数

}
void Scene0Collision()//场景1的碰撞检测
{

}
void Scene1Collision()//场景2的碰撞检测
{
	for (int i = 1; i < 50; i++)//检测子弹是否超出屏幕
	{
		//检测敌人的子弹是否击中玩家
			if (g_nDiraction == 1)
			{
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpRHorgun))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpRHorNormal))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpRUpgun))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpRUpNorlmal))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerRunR))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpRHorgun))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpRHorNormal))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpRUpgun))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpRUpNorlmal))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}

					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerRunR))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
				
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpRHorgun))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpRHorNormal))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpRUpgun))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpRUpNorlmal))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}

					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerRunR))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
				
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpRHorgun))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpRHorNormal))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpRUpgun))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpRUpNorlmal))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerRunR))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}

			}
			if (g_nDiraction == 2)
			{
				if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerRunL))
				{
					SpriteEnmyBullet[i].m_visible = false;
					g_PlayerBloodSolid.right -= 2;
				}
				
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpLHorgun))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpLHorNormal))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpLUpgun))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBullet[i], &SpritePlayerUpLUpNorlmal))
					{
						SpriteEnmyBullet[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
				
				
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpLHorgun))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpLHorNormal))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpLUpgun))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerUpLUpNorlmal))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
				
					if (SpriteInSprite(&SpriteEnmyBulletL[i], &SpritePlayerRunL))
					{
						SpriteEnmyBulletL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}

					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpLHorgun))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpLHorNormal))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpLUpgun))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerUpLUpNorlmal))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletP[i], &SpritePlayerRunL))
					{
						SpriteEnmyBulletP[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpLHorgun))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpLHorNormal))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpLUpgun))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}
					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerUpLUpNorlmal))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 5;
					}

					if (SpriteInSprite(&SpriteEnmyBulletPL[i], &SpritePlayerRunL))
					{
						SpriteEnmyBulletPL[i].m_visible = false;
						g_PlayerBloodSolid.right -= 2;
					}
			}
		for (int k = 0; k < NUMENMY; k++)
		{
			if (EnmySate[k] == PeopleState::LIFE)
			{

				if (i < 10)
				{
					//检测手雷是否炸到敌人
					if (SpriteInSprite(&SpriteShouleiExplode[i], &SpriteEnmy[k]))
					{
						EnmySate[k] = PeopleState::DEAD;
						SpriteEnmy[k].m_visible = false;
						EnmyDead(1, k, false);
						g_socre += 50;
						break;
						
					}
					if (SpriteInSprite(&SpriteShouleiExplode[i], &SpriteEnmyL[k]))
					{
						EnmySate[k] = PeopleState::DEAD;
						SpriteEnmyL[k].m_visible = false;
						EnmyDead(2, k, false);
						g_socre += 50;
						break;
					}

				}

				if (i < NUMHOSTAGE)
				{
					if (SpriteInSprite(&SpriteOilJarExplode[i], &SpriteEnmy[k],1))
					{
						EnmySate[k] = PeopleState::DEAD;
						SpriteEnmy[k].m_visible = false;
						EnmyDead(1, k, false);
						g_socre += 10;
						break;
					}
					if (SpriteInSprite(&SpriteOilJarExplode[i], &SpriteEnmyL[k],1))
					{
						EnmySate[k] = PeopleState::DEAD;
						SpriteEnmyL[k].m_visible = false;
						EnmyDead(2, k, false);
						g_socre += 10;
						break;
					}

				}

				if (SpriteInSprite(&SpriteGunBullte[i], &SpriteEnmyL[k], SpriteGunBullte[i].m_index - 1))
				{
					EnmySate[k] = PeopleState::DEAD;
					SpriteGunBullte[i] = SpriteGunBullte[0];//重置子弹
					SpriteEnmyL[k].m_visible = false;
					EnmyDead(2, k,false);
					g_socre += 10;
					break;
				}
				
				if (SpriteInSprite(&SpriteGunBullteL[i], &SpriteEnmy[k], SpriteGunBullteL[i].m_index - 1))
				{
					EnmySate[k] = PeopleState::DEAD;
					SpriteGunBullteL[i] = SpriteGunBullteL[0];//重置子弹
					SpriteEnmy[k].m_visible = false;
					EnmyDead(1, k,false);
					g_socre += 10;
					break;
				}

				if (SpriteInSprite(&SpriteDDBullte[i], &SpriteEnmyL[k], SpriteDDBullte[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					EnmySate[k] = PeopleState::DEAD;
					SpriteDDBullte[i] = SpriteDDBullte[0];//重置子弹
					SpriteDDBullteW[i] = SpriteDDBullteW[0];
					DDExplode(i, SpriteEnmyL[k].m_x[0], SpriteEnmyL[k].m_y[0]);
					SpriteEnmyL[k].m_visible = false;
					EnmyDead(2, k,false);
					g_socre += 10;
					break;
				}

				if (SpriteInSprite(&SpriteDDBullteL[i], &SpriteEnmy[k], SpriteDDBullteL[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					EnmySate[k] = PeopleState::DEAD;
					SpriteDDBullteL[i] = SpriteDDBullteL[0];//重置子弹
					SpriteDDBullteWL[i] = SpriteDDBullteWL[0];
					DDExplode(i, SpriteEnmy[k].m_x[0], SpriteEnmy[k].m_y[0]);
					SpriteEnmy[k].m_visible = false;
					EnmyDead(1, k,false);
					g_socre += 10;
					break;
				}

				
			}

			if (EnmyPSate[k] == PeopleState::LIFE)
			{
				if (i < 10)
				{
					if (SpriteInSprite(&SpriteShouleiExplode[i], &SpriteEnmyP[k]))
					{
						EnmyPSate[k] = PeopleState::DEAD;
						SpriteEnmyP[k].m_visible = false;
						EnmyDead(1, k, true);
						g_socre += 50;
						break;
					}
					if (SpriteInSprite(&SpriteShouleiExplode[i], &SpriteEnmyPL[k]))
					{
						EnmyPSate[k] = PeopleState::DEAD;
						SpriteEnmyPL[k].m_visible = false;
						EnmyDead(2, k, true);
						g_socre += 50;
						break;
					}

				}
				if (i < NUMHOSTAGE)
				{
					if (SpriteInSprite(&SpriteOilJarExplode[i], &SpriteEnmyP[k],1))
					{
						EnmyPSate[k] = PeopleState::DEAD;
						SpriteEnmyP[k].m_visible = false;
						EnmyDead(1, k, true);
						g_socre += 10;
						break;
					}
					if (SpriteInSprite(&SpriteOilJarExplode[i], &SpriteEnmyPL[k],1))
					{
						EnmyPSate[k] = PeopleState::DEAD;
						SpriteEnmyPL[k].m_visible = false;
						EnmyDead(2, k, true);
						g_socre += 10;
						break;
					}
				}
				if (SpriteInSprite(&SpriteGunBullte[i], &SpriteEnmyPL[k], SpriteGunBullte[i].m_index - 1))
				{
					EnmyPSate[k] = PeopleState::DEAD;
					SpriteGunBullte[i] = SpriteGunBullte[0];//重置子弹
					SpriteEnmyPL[k].m_visible = false;
					EnmyDead(2, k,true);
					g_socre += 10;
					break;
				}

				if (SpriteInSprite(&SpriteGunBullteL[i], &SpriteEnmyP[k], SpriteGunBullteL[i].m_index - 1))
				{
					EnmyPSate[k] = PeopleState::DEAD;
					SpriteGunBullteL[i] = SpriteGunBullteL[0];//重置子弹
					SpriteEnmyP[k].m_visible = false;
					EnmyDead(1, k,true);
					g_socre += 10;
					break;
				}

				if (SpriteInSprite(&SpriteDDBullte[i], &SpriteEnmyPL[k], SpriteDDBullte[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					EnmyPSate[k] = PeopleState::DEAD;
					SpriteDDBullte[i] = SpriteDDBullte[0];//重置子弹
					SpriteDDBullteW[i] = SpriteDDBullteW[0];//重置子弹
					DDExplode(i, SpriteEnmyPL[k].m_x[0], SpriteEnmyPL[k].m_y[0]);
					SpriteEnmyPL[k].m_visible = false;
					EnmyDead(2, k,true);
					g_socre += 10;
					break;
				}

				if (SpriteInSprite(&SpriteDDBullteL[i], &SpriteEnmyP[k], SpriteDDBullteL[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					EnmyPSate[k] = PeopleState::DEAD;
					SpriteDDBullteL[i] = SpriteDDBullteL[0];//重置子弹
					SpriteDDBullteWL[i] = SpriteDDBullteWL[0];//重置子弹
					DDExplode(i, SpriteEnmyPL[k].m_x[0], SpriteEnmyPL[k].m_y[0]);
					SpriteEnmyP[k].m_visible = false;
					EnmyDead(1, k,true);
					g_socre += 10;
					break;
				}
			}
		}
		/*if (Weapons == Weapon::GUN)
		{*/
			if (SpriteGunBullte[i].m_x[SpriteGunBullte->m_index - 1] >= WIDTH - 25)
				SpriteGunBullte[i] = SpriteGunBullte[0];
			if (SpriteGunBullteL[i].m_x[SpriteGunBullteL->m_index - 1] <= 8)
				SpriteGunBullteL[i] = SpriteGunBullteL[0];
			if (SpriteGunBullteU[i].m_y[SpriteGunBullteU->m_index - 1] <= 2)
				SpriteGunBullteU[i] = SpriteGunBullteU[0];
			for (int k = 0; k < NUMHOSTAGE; k++)
			{
				//检测手枪是否命中人质
				if (SpriteInSprite(&SpriteGunBullte[i], &SpriteHostage[k], SpriteGunBullte[i].m_index - 1))
				{
					SpriteGunBullte[i].m_visible = false;
					SpriteGunBullte[i] = SpriteGunBullte[0];//重置子弹
					SpriteHostage[k].m_visible = false;
					SpriteHostageRescue[k].m_visible = true;
					SpriteHostageRescue[k].m_mode = SpriteMode::ONCEFADE;
					SpriteHostageRun[k].addPlayDelay(1550, SpriteMode::ALLTIME);
					SpriteHostageGive[k].addPlayDelay(400, SpriteMode::ONCEFADE);
					SpriteHostageGift[k].addPlayDelay(800, SpriteMode::ALLTIME);
					/*int *nflg = new int;
					*nflg = Musicflg::GUNSHOOT;
					g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					g_socre += 50;
				}
				if (SpriteInSprite(&SpriteGunBullteL[i], &SpriteHostage[k], SpriteGunBullteL[i].m_index - 1))
				{
					SpriteGunBullteL[i].m_visible = false;
					SpriteGunBullteL[i] = SpriteGunBullteL[0];//重置子弹
					SpriteHostage[k].m_visible = false;
					SpriteHostageRescue[k].m_visible = true;
					SpriteHostageRescue[k].m_mode = SpriteMode::ONCEFADE;
					SpriteHostageRun[k].addPlayDelay(1550, SpriteMode::ALLTIME);
					SpriteHostageGive[k].addPlayDelay(400, SpriteMode::ONCEFADE);
					SpriteHostageGift[k].addPlayDelay(800, SpriteMode::ALLTIME);
					/*int *nflg = new int;
					*nflg = Musicflg::GUNSHOOT;
					g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					g_socre += 50;
				}
				//检查手枪是否命中油罐
				if (SpriteInSprite(&SpriteGunBullte[i], &SpriteOilJar[k], SpriteGunBullte[i].m_index - 1))
				{
					g_GunShootOil[k]++;
					SpriteGunBullte[i].m_visible = false;
					SpriteGunBullte[i] = SpriteGunBullte[0];//重置子弹
					if (g_GunShootOil[k] >= 5)
					{
						Musicflgs = Musicflg::SHOULEI;//播放音乐
						SpriteOilJar[k].m_visible = false;
						SpriteOilJarExplode[k].m_visible = true;
						SpriteOilJarExplode[k].m_mode = SpriteMode::ONCEFADE;
						SpriteOilJarScrap[k].addPlayDelay(300, SpriteMode::ALLTIME);
						g_socre += 10;
						/*int *nflg = new int;
						*nflg = Musicflg::SHOULEI;
						g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					}
					
				}

				if (SpriteInSprite(&SpriteGunBullteL[i], &SpriteOilJar[k], SpriteGunBullteL[i].m_index - 1))
				{
					g_GunShootOil[k]++;
					SpriteGunBullteL[i].m_visible = false;
					SpriteGunBullteL[i] = SpriteGunBullte[0];//重置子弹
					if (g_GunShootOil[k] >= 5)
					{
						Musicflgs = Musicflg::SHOULEI;//播放音乐
						SpriteOilJar[k].m_visible = false;
						SpriteOilJarExplode[k].m_visible = true;
						SpriteOilJarExplode[k].m_mode = SpriteMode::ONCEFADE;
						SpriteOilJarScrap[k].addPlayDelay(300, SpriteMode::ALLTIME);
						g_socre += 10;
						/*int *nflg = new int;
						*nflg = Musicflg::SHOULEI;
						g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					}
					
				}
			}
		/*}
		else if (Weapons == Weapon::DD)
		{*/
			if (SpriteDDBullte[i].m_x[SpriteDDBullte->m_index - 1] >= WIDTH - 50)
			{
				SpriteDDBullte[i] = SpriteDDBullte[0];
				SpriteDDBullteW[i] = SpriteDDBullteW[0];
			}
			if (SpriteDDBullteL[i].m_x[SpriteDDBullteL->m_index - 1] <= 8)
			{
				SpriteDDBullteL[i] = SpriteDDBullteL[0];
				SpriteDDBullteWL[i] = SpriteDDBullteWL[0];
			}
			if (SpriteDDBullteU[i].m_y[SpriteDDBullteU->m_index - 1] <= 2)
			{
				SpriteDDBullteU[i] = SpriteDDBullteU[0];
				SpriteDDBullteWU[i] = SpriteDDBullteWU[0];
			}
				
			for (int k = 0; k < NUMHOSTAGE; k++)
			{
				//检测导弹是否命中人质
				if (SpriteInSprite(&SpriteDDBullte[i], &SpriteHostage[k], SpriteDDBullte[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					SpriteDDBullte[i].m_visible = false;
					SpriteDDBullte[i] = SpriteDDBullte[0];//重置子弹
					SpriteDDBullteW[i].m_visible = false;
					SpriteDDBullteW[i] = SpriteDDBullteW[0];//重置子弹

					SpriteHostage[k].m_visible = false;
					SpriteHostageRescue[k].m_visible = true;
					SpriteHostageRescue[k].m_mode = SpriteMode::ONCEFADE;
					SpriteHostageRun[k].addPlayDelay(1550, SpriteMode::ALLTIME);
					SpriteHostageGive[k].addPlayDelay(400, SpriteMode::ONCEFADE);
					SpriteHostageGift[k].addPlayDelay(800, SpriteMode::ALLTIME);
					DDExplode(i, SpriteHostageRescue[k].m_x[0], SpriteHostageRescue[k].m_y[0]);
					/*int *nflg = new int;
					*nflg = Musicflg::GUNSHOOT;
					g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					g_socre += 50;
				}
				if (SpriteInSprite(&SpriteDDBullteL[i], &SpriteHostage[k], SpriteDDBullteL[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					SpriteDDBullteL[i].m_visible = false;
					SpriteDDBullteL[i] = SpriteDDBullteL[0];//重置子弹
					SpriteDDBullteWL[i].m_visible = false;
					SpriteDDBullteWL[i] = SpriteDDBullteWL[0];//重置子弹
					SpriteHostage[k].m_visible = false;
					SpriteHostageRescue[k].m_visible = true;
					SpriteHostageRescue[k].m_mode = SpriteMode::ONCEFADE;
					SpriteHostageRun[k].addPlayDelay(1550, SpriteMode::ALLTIME);
					SpriteHostageGive[k].addPlayDelay(400, SpriteMode::ONCEFADE);
					SpriteHostageGift[k].addPlayDelay(800, SpriteMode::ALLTIME);
					DDExplode(i, SpriteHostageRescue[k].m_x[0], SpriteHostageRescue[k].m_y[0]);
					/*int *nflg = new int;
					*nflg = Musicflg::GUNSHOOT;
					g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					g_socre += 50;
				}
				//检查导弹是否命中油罐
				if (SpriteInSprite(&SpriteDDBullte[i], &SpriteOilJar[k], SpriteDDBullte[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					SpriteDDBullte[i].m_visible = false;
					SpriteDDBullte[i] = SpriteDDBullte[0];//重置子弹
					SpriteDDBullteW[i].m_visible = false;
					SpriteDDBullteW[i] = SpriteDDBullteW[0];//重置子弹

					DDExplode(i, SpriteOilJar[k].m_x[0], SpriteOilJar[k].m_y[0]);
					SpriteOilJar[k].m_visible = false;
					SpriteOilJarExplode[k].m_visible = true;
					SpriteOilJarExplode[k].m_mode = SpriteMode::ONCEFADE;
					SpriteOilJarScrap[k].addPlayDelay(300, SpriteMode::ALLTIME);
					/*int *nflg = new int;
					*nflg = Musicflg::GUNSHOOT;
					g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					g_socre += 30;
				}
				if (SpriteInSprite(&SpriteDDBullteL[i], &SpriteOilJar[k], SpriteDDBullteL[i].m_index - 1))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					SpriteDDBullteL[i].m_visible = false;
					SpriteDDBullteL[i] = SpriteDDBullteL[0];//重置子弹
					SpriteDDBullteWL[i].m_visible = false;
					SpriteDDBullteWL[i] = SpriteDDBullteWL[0];//重置子弹

					DDExplode(i, SpriteOilJar[k].m_x[0], SpriteOilJar[k].m_y[0]);
					SpriteOilJar[k].m_visible = false;
					SpriteOilJarExplode[k].m_visible = true;
					SpriteOilJarExplode[k].m_mode = SpriteMode::ONCEFADE;
					SpriteOilJarScrap[k].addPlayDelay(300, SpriteMode::ALLTIME);
					/*int *nflg = new int;
					*nflg = Musicflg::GUNSHOOT;
					g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					g_socre += 30;
				}
			}
		//}
		//检测导弹是否命中飞机
			if (SpriteInSprite(&SpriteDDBullteU[i], &SpritePlane))
			{
				Musicflgs = Musicflg::SHOULEI;//播放音乐
				//SpriteDDBullteU[i].m_visible = false;
				DDExplode(i, SpriteDDBullteU[i].m_x[0]-30, SpritePlane.m_y[0]);
				SpriteDDBullteU[i] = SpriteDDBullteU[0];
				SpriteDDBullteWU[i] = SpriteDDBullteWU[0];
				g_PlaneBlood -= 100;
				g_socre += 20;
			}
			if (SpriteInSprite(&SpriteGunBullteU[i], &SpritePlane))
			{
				g_socre += 5;
				SpriteGunBullteU[i] = SpriteGunBullteU[0];
				g_PlaneBlood -= 10;
			}
		
		for (int k = 0; k < NUMHOSTAGE; k++)
		{
			
			if (i < 10)
			{
				//检测手雷是否炸到人质
				if (SpriteInSprite(&SpriteShouleiExplode[i], &SpriteHostage[k]))
				{
					SpriteHostage[k].m_visible = false;//找到就播放一系列人质解救动画
					SpriteHostageRescue[k].m_visible = true;
					SpriteHostageRescue[k].m_mode = SpriteMode::ONCEFADE;
					SpriteHostageRun[k].addPlayDelay(1550, SpriteMode::ALLTIME);
					SpriteHostageGive[k].addPlayDelay(400, SpriteMode::ONCEFADE);
					SpriteHostageGift[k].addPlayDelay(800, SpriteMode::ALLTIME);
					g_socre += 50;
				}
				//检查手雷是否炸到油罐
				if (SpriteInSprite(&SpriteShouleiExplode[i], &SpriteOilJar[k]))
				{
					Musicflgs = Musicflg::SHOULEI;//播放音乐
					SpriteOilJar[k].m_visible = false;
					SpriteOilJarExplode[k].m_visible = true;
					SpriteOilJarExplode[k].m_mode = SpriteMode::ONCEFADE;
					SpriteOilJarScrap[k].addPlayDelay(300, SpriteMode::ALLTIME);
					/*int *nflg = new int;
					*nflg = Musicflg::GUNSHOOT;
					g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
					g_socre += 30;
				}
				
			}
			//检查人质是否跑出屏幕
			if (SpriteHostage[k].m_x[0] <= -500)
			{
				SpriteHostage[k].m_visible = false;
			}
			//检查主角是否吃到水果
			if (SpriteInSprite(&SpritePlayerDownR, &SpriteHostageGift[k]))
			{
				/*int *pflg = new int;
				*pflg = Musicflg::PICK;
				GameMusic(pflg);*/
				Musicflgs = Musicflg::PICK;
				g_socre += 100;
				g_PlayerBloodSolid.right += 5;
				SpriteHostageGift[k].m_visible = false;
				
			}
			if (SpriteInSprite(&SpritePlayerDownL, &SpriteHostageGift[k]))
			{
				/*int *pflg = new int;
				*pflg = Musicflg::PICK;
				GameMusic(pflg);*/
				Musicflgs = Musicflg::PICK;
				g_socre += 100;
				g_PlayerBloodSolid.right += 5;
				SpriteHostageGift[k].m_visible = false;
				
			}
		}
			
	}

	for (int i = 0; i < 10; i++)//检测手雷是否掉地
	{
		if (SpriteShoulei[i].m_visible == true)
		{
			if (SpriteShoulei[i].m_tempindex == SpriteShoulei[i].m_index - 1)//显示且索引到最后一帧
			{
				Musicflgs = Musicflg::SHOULEI;//播放音乐
				ShouleiBoom(i, 1);//执行爆炸
				break;
			}
		}

	}
	for (int i = 0; i < 10; i++)//检测手雷是否掉地
	{
		if (SpriteShouleiL[i].m_visible == true)
		{
			if (SpriteShouleiL[i].m_tempindex == SpriteShouleiL[i].m_index - 1)
			{
				Musicflgs = Musicflg::SHOULEI;//播放音乐
				ShouleiBoom(i, 2);
				break;
			}
		}

	}
	
	

	
}
void Scene0KeyEvent()//场景1的按键处理
{

}
void Scene1KeyEvent()//场景2的按键处理
{
	/*根据按键播放相应的动画，巨复杂，根据反馈调成的，可读性巨低*/
	if (GetAsyncKeyState('D') < 0)//表示A键按下
	{
		if (GetAsyncKeyState('K') < 0)
		{
			if (!isJump)
				PlayerJump(g_nDiraction);
		}
		
			if (g_nDiraction == 2)//检测到向另一方则隐藏一方
			{
				g_nDiraction = 1;
				SpritePlayerRunL.m_visible = false;//一系列的无关的人物部分隐藏起来
				SpritePlayerUpLUpgun.m_visible = false;
				SpritePlayerUpLHorgun.m_visible = false;
				SpritePlayerUpLHorNormal.m_visible = false;
				SpritePlayerUpLUpNorlmal.m_visible = false;
				SpritePlayerDownL.m_visible = false;
			}
			PlayerForward();//人物向前移动
			if (GetAsyncKeyState('L') < 0)
			{
				PlayerShoulei();
			}
			else
			{
				if (GetAsyncKeyState('W') <0)
				{
					PlayerUp();
				}
				if (!isJump)
				{
					if (GetAsyncKeyState('J') < 0)
					{
						if (GetAsyncKeyState('W') < 0)
						{
							PlayerUpShoot();
						}
						else
							PlayerShoot(g_nDiraction);
					}
				}
				
			}
		
		/*g_startbuttondown = timeGetTime();//提供延迟
		g_buttondowmdelay = 130;*/
	}
	else if (GetAsyncKeyState('E') < 0)
	{
		
			if (timeGetTime() - g_starttime_button > g_button_delay)
			{
				if (Weapons == Weapon::GUN)
					Weapons = Weapon::DD;
				else if (Weapons == Weapon::DD)
					Weapons = Weapon::GUN;
				g_starttime_button = timeGetTime();
			}
		
		
		
	}
	else if (GetAsyncKeyState('A') < 0)
	{
		if (GetAsyncKeyState('K') < 0)
		{
			if(!isJump)
				PlayerJump(g_nDiraction);
		}
	
			if (g_nDiraction == 1)//检测到向另一方则隐藏一方
			{
				g_nDiraction = 2;
				SpritePlayerRunR.m_visible = false;
				SpritePlayerUpRUpgun.m_visible = false;
				SpritePlayerUpRHorgun.m_visible = false;
				SpritePlayerUpRHorNormal.m_visible = false;
				SpritePlayerUpRUpNorlmal.m_visible = false;
				SpritePlayerDownR.m_visible = false;
			}
			PlayerBack();
			if (GetAsyncKeyState('L') < 0)
			{
				PlayerShoulei();
			}
			else
			{
				if (GetAsyncKeyState('W') <0)
				{
					PlayerUp();
				}
				if (!isJump)
				{
					if (GetAsyncKeyState('J') < 0)
					{
						if (GetAsyncKeyState('W') < 0)
						{
							PlayerUpShoot();
						}
						else
							PlayerShoot(g_nDiraction);
					}
				}
			
			}
		/*g_startbuttondown = timeGetTime();//提供延迟
		g_buttondowmdelay = 130;*/
	}
	else if (GetAsyncKeyState('W') < 0)
	{
		
			if (timeGetTime() - g_startbuttondown >= g_buttondowmdelay)
			{
				PlayerUp();
				g_buttondowmdelay = 0;
			}

			if (GetAsyncKeyState('J') < 0)
			{
				PlayerUpShoot();
				g_startbuttondown = timeGetTime();//提供延迟
				g_buttondowmdelay = 240;
			}
			SpritePlayerDownR.Pause(0);
			SpritePlayerDownL.Pause(0);
		
		
	}
	else if (GetAsyncKeyState('J') < 0)
	{
		if (!isJump)
		{
			PlayerShoot(g_nDiraction);
			SpritePlayerDownL.Pause(0);
			SpritePlayerDownR.Pause(0);
			g_startbuttondown = timeGetTime();//提供延迟
			g_buttondowmdelay = 175;
		}
		
	}
	else if (GetAsyncKeyState('L') < 0)
	{
		if (!isJump)
		{
			PlayerShoulei();
			if (g_nDiraction == 1)
				SpritePlayerDownR.Pause(0);
			else if (g_nDiraction == 2)
				SpritePlayerDownL.Pause(0);
			g_startbuttondown = timeGetTime();//提供延迟
			g_buttondowmdelay = 360;
		}
		
	}
	else if (GetAsyncKeyState('K') < 0)
	{
		if(!isJump)
			PlayerJump(g_nDiraction);
	}
	else
	{
		if (timeGetTime() - g_startbuttondown >= g_buttondowmdelay)//在这里延迟，到了延迟才执行默认的动画
		{
			PlayerDefalut(g_nDiraction);
			g_buttondowmdelay = 0;
		}
		
	}
	if (GetAsyncKeyState(VK_ESCAPE))
	{
		if (MessageBox(g_hWnd, "是否存档", "提示", MB_YESNO | MB_ICONINFORMATION) == IDYES)
		{
			FILE* storefile = fopen("resources\\store.st", "wb");
			if (storefile == NULL)
			{
				MessageBox(g_hWnd, "打开存档文件失败", "错误", MB_OK);
			}
			else
			{
				fwrite(&g_GameEngine.m_sceneID, 1, sizeof(int), storefile);
				fwrite(&Scene1.m_bkx, 1, sizeof(int), storefile);
				fclose(storefile);
			}
		}
	}
}
void Scene0MouseEvent(MOUSEMSG msg)//场景1的鼠标消息处理
{
	POINT pt = { msg.x,msg.y };
	if (msg.uMsg == WM_LBUTTONDOWN)
	{
		if (PtInRect(&g_reStart, pt))//检测开始按钮
		{
			g_scene = 2;
			g_GameEngine.m_sceneID = g_scene;
			Scene2Start();
			/*CloseScene0();
			Scene1Start();
			GameMusic(Musicflg::DEFAULT);*/
		}
		else if (PtInRect(&g_reEnd, pt))//检测退出按钮
		{
			exit(0);
		}
		else if (PtInRect(&g_Statement, pt))//检测操作说明按钮
		{
			g_scene = 2;
			g_GameEngine.m_sceneID = g_scene;//改变场景号
			Scene2Start();
		}
		else if(PtInRect(&g_store,pt))
		{
			char content[100] = { 0 };
			FILE* storefile = fopen("resources\\store.st", "rb");
			if (storefile == NULL)
			{
				MessageBox(g_hWnd, "打开存档失败", "错误", MB_OK | MB_ICONERROR);
				return;
			}
			else
			{
				/*int scID=0, scX=0;
				char sceneID = fgetc(storefile);
				fgets(content, 99, storefile);
				char *scenex,*sceney;
				scenex = content + 1;
				sceney = strstr(content + 1, " ") + 1;
				char x[5] = { 0 };
				strncpy(x, scenex, sceney - scenex-1);
				scID = sceneID - '0';
				//MessageBox(g_hWnd, x, x, MB_OK);
				int n = strlen(x);
				for (int i = 0; i < n; i++)
				{
					if (x[i] != '-')
					{
						scX += (x[i]-'0') * pow(10, n-i-1);
					}
				}
				if (x[0] == '-')
					scX = -scX;*/
				int scID=0, scX=0;
				if (fread(&scID, 1, sizeof(int), storefile) == 0)
				{
					MessageBox(g_hWnd, "还没有存档", "提示", MB_OK | MB_ICONINFORMATION);
					fclose(storefile);
					return;
				}
				fread(&scX, 1, sizeof(int), storefile);
				if (scID == 1)
				{
					g_scene = 1;
					g_GameEngine.m_sceneID = g_scene;//切换到场景1
					CloseScene0();//如函数命名
					CloseScene2();
					Scene1Start();
					Scene1.m_bkx = scX;
				}
				fclose(storefile);
			}
		}

	}
}
void Scene1MouseEvent(MOUSEMSG msg)//场景2的鼠标消息处理
{

}

DWORD WINAPI Scene0Music(void* nflg)//场景1音乐
{
	//根据相应的音乐标志加载播放音乐
	int *pnflg = (int*)nflg;
	/*switch ((*pnflg))
	{
	case Musicflg::BKMUSIC1:
		mciSendString("close bkstart", NULL, 0, NULL);
		mciSendString("open resources\\music\\bkstart.mp3 alias bkstart", NULL, 0, NULL);
		mciSendString("play bkstart repeat", NULL, 0, NULL);
		break;
	case Musicflg::DEFAULT:
		mciSendString("open resources\\music\\bkstart.mp3 alias bkstart", NULL, 0, NULL);
		mciSendString("play bkstart repeat", NULL, 0, NULL);
		break;
	}*/
	delete pnflg;
	return 0;
}
DWORD WINAPI Scene1Music(void* nflg)//场景2音乐
{
	int *pnflg = (int*)nflg;
	/*switch (*pnflg)
	{
	case Musicflg::PICK:
		mciSendString("close pick", NULL, 0, NULL);
		mciSendString("open resources\\music\\pick.mp3 alias pick", NULL, 0, NULL);
		mciSendString("play pick", NULL, 0, NULL);
		break;
	case Musicflg::XIU:
		mciSendString("close xiu", NULL, 0, NULL);
		mciSendString("open resources\\music\\bombFall.mp3 alias xiu", NULL, 0, NULL);
		mciSendString("play xiu", NULL, 0, NULL);
		break;
	case Musicflg::BKMUSIC2:
		mciSendString("open resources\\music\\bkmain.mp3 alias bkmain", NULL, 0, NULL);
		mciSendString("play bkmain repeat", NULL, 0, NULL);
		break;
	case Musicflg::MISSIONSTART:
		mciSendString("open resources\\music\\Mission1Start.wav alias mission1start", NULL, 0, NULL);
		mciSendString("play mission1start", NULL, 0, NULL);
		break;
	case Musicflg::DEFAULT:
		mciSendString("open resources\\music\\bkmain.mp3 alias bkmain", NULL, 0, NULL);
		mciSendString("open resources\\music\\Mission1Start.wav alias mission1start", NULL, 0, NULL);
		mciSendString("play bkmain repeat", NULL, 0, NULL);
		mciSendString("play mission1start", NULL, 0, NULL);
		break;
	case Musicflg::MISSIONCOMPLETE:
		mciSendString("close missioncomplete", NULL, 0, NULL);
		mciSendString("open resources\\music\\MissionComplete.wav alias missioncomplete", NULL, 0, NULL);
		mciSendString("play missioncomplete", NULL, 0, NULL);
		break;
	case Musicflg::PLAYERDEAD:
		mciSendString("close playerdead", NULL, 0, NULL);
		mciSendString("open resources\\music\\soliderDie.mp3 alias playerdead", NULL, 0, NULL);
		mciSendString("play playerdead", NULL, 0, NULL);
		break;
	case Musicflg::ENMYDEAD:
		mciSendString("close enmydead", NULL, 0, NULL);
		mciSendString("open resources\\music\\enemyDie.mp3 alias enmydead", NULL, 0, NULL);
		mciSendString("play enmydead", NULL, 0, NULL);
		break;
	case Musicflg::GUNSHOOT:
		mciSendString("close gunshoot", NULL, 0, NULL);
		mciSendString("open resources\\music\\shoot.mp3 alias gunshoot", NULL, 0, NULL);
		mciSendString("play gunshoot", NULL, 0, NULL);
		break;
	case Musicflg::SHOULEI:
		mciSendString("close shouleiboom",NULL,0,NULL);
		mciSendString("open resources\\music\\shoulei.mp3 alias shouleiboom", NULL, 0, NULL);
		mciSendString("play shouleiboom", NULL, 0, NULL);
		break;
	case Musicflg::PLANEBOMB:
		mciSendString("close planebomb", NULL, 0, NULL);
		mciSendString("open resources\\music\\jetExplosion.mp3 alias planebomb", NULL, 0, NULL);
		mciSendString("play planebomb", NULL, 0, NULL);
		break;
	}*/
	delete pnflg;//删除分配的内存
	return 0;
}

void CloseScene0()//关闭场景1
{
	CloseScene0Music();
	for (int i = 0; i < 3; i++)
		g_Pang[i].DeleteAll();
	Scene0.DeleteAll();//释放0的资源
}

void Scene0Start()//初始化场景1
{
	Musicflgs = Musicflg::ALLSTART0;
	char CurrentDir[260];//用来储存格式化的文件路径，分别是原图和蒙版
	char CurrentDir1[260];
	//GetCurrentDirectory(260, g_CurrentDir);
	g_CurrentDir[0] = 0;
	strcat(g_CurrentDir, "resources\\images%s");
	//对场景1初始化数据
	g_reStart.left = 82;
	g_reStart.top = 224;//开始按钮的位置
	g_reStart.right = 208;
	g_reStart.bottom = 252;

	g_reEnd.left = 84;
	g_reEnd.top = 284;
	g_reEnd.right = 137;//结束按钮的位置
	g_reEnd.bottom = 307;

	g_Statement.left=16;//操作说明的位置
	g_Statement.top=25;
	g_Statement.right=143;
	g_Statement.bottom=51;

	g_store.left = 373;//存档的位置
	g_store.top = 335;
	g_store.right = 486;
	g_store.bottom = 356;

	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image52.jpg");
	IMAGE* Bkimage1Scene0 = new IMAGE;
	loadimage(Bkimage1Scene0, CurrentDir);
	Scene0.AddBk(Bkimage1Scene0);	
	Scene0.Add(&g_Pang[0]);//添加标签到场景中
	Scene0.Add(&g_Pang[1]);
	Scene0.Add(&g_Pang[2]);
	Scene0.Add(&g_MilltaryRescue);
	g_Pang[0].Init(60, 150, 1, 0, 0, 1);
	g_Pang[1].Init(120, 120, 1, 0, 0, 1);
	g_Pang[2].Init(200, 94, 1, 0, 0, 1);
	//加载资源到相应的精灵中
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image235.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image235_mask.bmp");
	g_Pang[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image237.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image237_mask.bmp");
	g_Pang[1].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image239.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image239_mask.bmp");
	g_Pang[2].Add(LoadImageElement(CurrentDir, CurrentDir1));

	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1122.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1122_mask.bmp");
	g_MilltaryRescue.Add(LoadImageElement(CurrentDir, CurrentDir1));
	g_MilltaryRescue.Init(150, 15, 1, 0, 0, 1);
#ifndef _DEBUG//调试的时候不播放音乐，难受
	/*int *nflg = new int;
	*nflg = Musicflg::DEFAULT;
	GameMusic(nflg);//播放背景音乐*/
#endif // !DEBUG
}
void Scene1Start()//初始化场景2
{
	settextstyle(40, 20, "黑体");
	char startwaiting[30] = "正在加载资源,请稍后........";
	outtextxy(10,200,startwaiting);
	//Musicflgs = Musicflg::ALLSTART1;
	srand(timeGetTime());
	isJump = false;
	
	char CurrentDir[260] = { 0 };//用来储存格式化的文件路径，分别是原图和蒙版
	char CurrentDir1[260] = { 0 };
	char CurrentDirBk[260] = { 0 };
	//GetCurrentDirectory(260, g_CurrentDir);


	//初始化用于控制敌人的死亡动画
	g_EnmydeadNum = 1;
	g_EnmydeadNumL = 1;

	g_EnmybulletNum = 1;
	g_EnmybulletNumP = 1;
	g_EnmybulletNumL = 1;
	g_EnmybulletNumPL = 1;

	g_starttime_button = 0;//用来控制某些只需要按下一次就好的键，防止极短时间内的一直按住
	g_button_delay = 100;

	g_PlaneBlood = 20000;
	g_PlaneBombNum = 1;
	g_PlaneBomDelay=500;
	g_PlaneBombStarttime=timeGetTime();

	//初始化分数
	g_socre = 0;

	Weapons = Weapon::GUN;//初始化武器标志

	//初始化子弹相关
	g_NumBullte = 1;
	g_bulletinteral = 300;
	g_bulletstarttime = timeGetTime();

	g_NumBullteL = 1;
	g_bulletinteralL = 300;
	g_bulletstarttimeL = timeGetTime();

	g_NumBullteU = 1;
	g_bulletinteralU = 300;
	g_bulletstarttimeU = timeGetTime();

	g_ShouleiInternal=400;
	g_ShouleiStarttime=timeGetTime();
	g_ShouleiInternalL=400;
	g_ShouleiStarttimeL=timeGetTime();

	g_ShouleiNum = 1;
	g_ShouleiNumL = 1;
	g_ShouleiExplodeNum = 1;

	/*对场景2进行初始化*/
	//加载背景图片
	/*接下来是大量的资源加载，大量的重复*/
	IMAGE* imageBk1 = new IMAGE;
	IMAGE* imageBk2 = new IMAGE;
	IMAGE* imageBk3 = new IMAGE;
	IMAGE* imageBk4 = new IMAGE;
	IMAGE* imageBk5 = new IMAGE;
	IMAGE* imageBk6 = new IMAGE;
	IMAGE* imageBk7 = new IMAGE;
	IMAGE* imageBk8 = new IMAGE;

	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image259.jpg");
	loadimage(imageBk1, CurrentDirBk);
	Scene1.AddBk(imageBk1);
	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image261.jpg");
	loadimage(imageBk2, CurrentDirBk);
	Scene1.AddBk(imageBk2);
	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image263.jpg");
	loadimage(imageBk3, CurrentDirBk);
	Scene1.AddBk(imageBk3);
	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image265.jpg");
	loadimage(imageBk4, CurrentDirBk);
	Scene1.AddBk(imageBk4);
	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image267.jpg");
	loadimage(imageBk5, CurrentDirBk);
	Scene1.AddBk(imageBk5);
	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image269.jpg");
	loadimage(imageBk6, CurrentDirBk);
	Scene1.AddBk(imageBk6);
	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image271.jpg");
	loadimage(imageBk7, CurrentDirBk);
	Scene1.AddBk(imageBk7);
	sprintf(CurrentDirBk, g_CurrentDir, "\\bk\\image273.jpg");
	loadimage(imageBk8, CurrentDirBk);
	Scene1.AddBk(imageBk8);

	//加载人物图片
	Scene1.Add(&g_MissionStart);

	//Scene1.Add(&SpriteEnmy[0]);
	g_MissionStart.Init(156, 161, 1, 0, 0, 1);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\missionstart.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\missionstart_mask.bmp");
	g_MissionStart.Add(LoadImageElement(CurrentDir, CurrentDir1));

	//加载主角图片
	//添加到场景
	Scene1.Add(&SpritePlane);
	Scene1.Add(&SpritePlayerShoulei);
	Scene1.Add(&SpritePlayerShouleiL);
	for (int i = 0; i < 50; i++)
	{

		if (i < NUMHOSTAGE)
		{
			Scene1.Add(&SpriteHostage[i]);
			Scene1.Add(&SpriteHostageRescue[i]);
			Scene1.Add(&SpriteHostageRun[i]);
			Scene1.Add(&SpriteHostageGive[i]);
			Scene1.Add(&SpriteHostageGift[i]);
			Scene1.Add(&SpriteOilJarScrap[i]);
			Scene1.Add(&SpriteOilJar[i]);
			Scene1.Add(&SpriteOilJarExplode[i]);
			g_GunShootOil[i] = 0;
		}

		Scene1.Add(&SpriteGunBullte[i]);
		Scene1.Add(&SpriteGunBullteL[i]);
		Scene1.Add(&SpriteGunBullteU[i]);
		Scene1.Add(&SpriteDDBullte[i]);
		Scene1.Add(&SpriteDDBullteL[i]);
		Scene1.Add(&SpriteDDBullteU[i]);
		Scene1.Add(&SpriteDDBullteW[i]);
		Scene1.Add(&SpriteDDBullteWL[i]);
		Scene1.Add(&SpriteDDBullteWU[i]);
		Scene1.Add(&SpritePlaneBomb[i]);
		Scene1.Add(&SpritePlaneBombExplode[i]);

		
		
		if (i < 10)
		{
			Scene1.Add(&SpriteShoulei[i]);
			Scene1.Add(&SpriteShouleiL[i]);
			Scene1.Add(&SpriteShouleiExplode[i]);
			
		}
		if (i < 13)
		{
			Scene1.Add(&SpriteSocre0[i]);
			Scene1.Add(&SpriteSocre1[i]);
			Scene1.Add(&SpriteSocre2[i]);
			Scene1.Add(&SpriteSocre3[i]);
			Scene1.Add(&SpriteSocre4[i]);
			Scene1.Add(&SpriteSocre5[i]);
			Scene1.Add(&SpriteSocre6[i]);
			Scene1.Add(&SpriteSocre7[i]);
			Scene1.Add(&SpriteSocre8[i]);
			Scene1.Add(&SpriteSocre9[i]);
		}
		Scene1.Add(&SpriteDDExplode[i]);

		Scene1.Add(&SpriteEnmyBullet[i]);
		Scene1.Add(&SpriteEnmyBulletP[i]);
		Scene1.Add(&SpriteEnmyBulletL[i]);
		Scene1.Add(&SpriteEnmyBulletPL[i]);
	}

	for (int i = 0; i < NUMENMY; i++)
	{
		Scene1.Add(&SpriteEnmy[i]);
		Scene1.Add(&SpriteEnmyL[i]);
		Scene1.Add(&SpriteEnmyP[i]);
		Scene1.Add(&SpriteEnmyPL[i]);
		Scene1.Add(&SpriteEnmyDead[i]);
		Scene1.Add(&SpriteEnmyDeadL[i]);
		Scene1.Add(&SpriteEnmyDeadP[i]);
		Scene1.Add(&SpriteEnmyDeadPL[i]);
		EnmySate[i] = PeopleState::LIFE;
		EnmyPSate[i] = PeopleState::LIFE;
	}

	Scene1.Add(&SpritePlayerDownL);
	Scene1.Add(&SpritePlayerDownR);
	Scene1.Add(&SpritePlayerUpLUpNorlmal);
	Scene1.Add(&SpritePlayerUpLHorNormal);
	Scene1.Add(&SpritePlayerUpRUpNorlmal);
	Scene1.Add(&SpritePlayerUpRHorNormal);
	Scene1.Add(&SpritePlayerUpLUpgun);
	Scene1.Add(&SpritePlayerUpLHorgun);
	Scene1.Add(&SpritePlayerUpRUpgun);
	Scene1.Add(&SpritePlayerUpRHorgun);
	Scene1.Add(&SpritePlayerRunR);
	Scene1.Add(&SpritePlayerRunL);
	
	/*接下来是各种加载到吐的资源*/

	//加载向右的图
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1334.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1334_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[0] = 120;
	SpritePlayerDownR.m_y[0] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1337.bmp");		//下半部分
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1337_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[1] = 120;
	SpritePlayerDownR.m_y[1] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1339.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1339_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[2] = 110;
	SpritePlayerDownR.m_y[2] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1341.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1341_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[3] = 106;
	SpritePlayerDownR.m_y[3] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1343.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1343_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[4] = 110;
	SpritePlayerDownR.m_y[4] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1345.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1345_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[5] = 120;
	SpritePlayerDownR.m_y[5] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1347.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1347_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[6] = 120;
	SpritePlayerDownR.m_y[6] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1349.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1349_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[7] = 110;
	SpritePlayerDownR.m_y[7] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1351.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1351_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[8] = 106;
	SpritePlayerDownR.m_y[8] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1353.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1353_mask.bmp");
	SpritePlayerDownR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownR.m_x[9] = 110;
	SpritePlayerDownR.m_y[9] = 300;
	SpritePlayerDownR.Init(120, 300, 1, 50, timeGetTime(), 10, true, SpriteMode::PAUSE,ImageMode::DEFFIRENT);

	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1383.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1383_mask.bmp");
	SpritePlayerUpRHorNormal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1385.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1385_mask.bmp");
	SpritePlayerUpRHorNormal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1387.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1387_mask.bmp");
	SpritePlayerUpRHorNormal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRHorNormal.Init(112, 258, 1, 270, timeGetTime(), 2,true,SpriteMode::PAUSE);


	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1415.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1415_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1417.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1417_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1419.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1419_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1421.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1421_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1423.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1423_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1425.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1425_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1427.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1427_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1429.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1429_mask.bmp");
	SpritePlayerRunR.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunR.Init(112, 258, 1, 50, timeGetTime(), 9,false,SpriteMode::PAUSE);


	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1684.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1684_mask.bmp");
	SpritePlayerUpRHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1686.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1686_mask.bmp");
	SpritePlayerUpRHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1688.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1688_mask.bmp");
	SpritePlayerUpRHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1690.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1690_mask.bmp");
	SpritePlayerUpRHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1692.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1692_mask.bmp");
	SpritePlayerUpRHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1694.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1694_mask.bmp");
	SpritePlayerUpRHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1696.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1696_mask.bmp");
	SpritePlayerUpRHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRHorgun.Init(113, 260, 1, 20, timeGetTime(), 7, false, SpriteMode::PAUSE);

	//加载子弹资源
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image2372.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image2372_mask.bmp");
	SpriteGunBullte[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullte[0].m_x[0] = 185;
	SpriteGunBullte[0].m_y[0] = 271;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image2374.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image2374_mask.bmp");
	SpriteGunBullte[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullte[0].m_x[1] = 190;
	SpriteGunBullte[0].m_y[1] = 276;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image2376.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image2376_mask.bmp");
	SpriteGunBullte[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullte[0].m_x[2] = 193;
	SpriteGunBullte[0].m_y[2] = 276;
	SpriteGunBullte[0].Init(185, 271, 1, 50, timeGetTime(), 3, false, SpriteMode::PAUSE,ImageMode::DEFFIRENT);
	//拷贝所有的子弹
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//加载向左的图
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1334.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1334_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[0] = 135;
	SpritePlayerDownL.m_y[0] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1337.bmp");		//下半部分
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1337_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[1] = 142;  
	SpritePlayerDownL.m_y[1] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1339.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1339_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[2] = 123;
	SpritePlayerDownL.m_y[2] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1341.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1341_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[3] = 119;
	SpritePlayerDownL.m_y[3] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1343.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1343_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[4] = 134;
	SpritePlayerDownL.m_y[4] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1345.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1345_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[5] = 135; 
	SpritePlayerDownL.m_y[5] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1347.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1347_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[6] = 142;
	SpritePlayerDownL.m_y[6] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1349.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1349_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[7] = 123;
	SpritePlayerDownL.m_y[7] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1351.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1351_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[8] = 119;
	SpritePlayerDownL.m_y[8] = 298;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1353.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1353_mask.bmp");
	SpritePlayerDownL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerDownL.m_x[9] = 134;
	SpritePlayerDownL.m_y[9] = 298;
	SpritePlayerDownL.Init(135, 298, 1, 50, timeGetTime(), 10, false, SpriteMode::PAUSE, ImageMode::DEFFIRENT);

	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1383.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1383_mask.bmp");
	SpritePlayerUpLHorNormal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1385.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1385_mask.bmp");
	SpritePlayerUpLHorNormal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1387.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1387_mask.bmp");
	SpritePlayerUpLHorNormal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLHorNormal.Init(112, 258, 1, 270, timeGetTime(), 2, false, SpriteMode::PAUSE);


	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1415.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1415_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[0] = 113;
	SpritePlayerRunL.m_y[0] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1417.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1417_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[1] = 113;
	SpritePlayerRunL.m_y[1] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1419.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1419_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[2] = 111;
	SpritePlayerRunL.m_y[2] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1421.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1421_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[3] = 113;
	SpritePlayerRunL.m_y[3] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1423.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1423_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[4] = 113;
	SpritePlayerRunL.m_y[4] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1425.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1425_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[5] = 106;
	SpritePlayerRunL.m_y[5] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1427.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1427_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[6] = 103;
	SpritePlayerRunL.m_y[6] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1429.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1429_mask.bmp");
	SpritePlayerRunL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerRunL.m_x[7] = 108;
	SpritePlayerRunL.m_y[7] = 258;
	SpritePlayerRunL.Init(113, 258, 1, 50, timeGetTime(), 9, false, SpriteMode::PAUSE,ImageMode::DEFFIRENT);


	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1684.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1684_mask.bmp");
	SpritePlayerUpLHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1686.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1686_mask.bmp");
	SpritePlayerUpLHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1688.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1688_mask.bmp");
	SpritePlayerUpLHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1690.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1690_mask.bmp");
	SpritePlayerUpLHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1692.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1692_mask.bmp");
	SpritePlayerUpLHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1694.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1694_mask.bmp");
	SpritePlayerUpLHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1696.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1696_mask.bmp");
	SpritePlayerUpLHorgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLHorgun.Init(85, 260, 1, 20, timeGetTime(), 7, false, SpriteMode::PAUSE);

	//加载子弹资源
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image2372.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image2372_mask.bmp");
	SpriteGunBullteL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullteL[0].m_x[0] = 80;
	SpriteGunBullteL[0].m_y[0] = 271;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image2374.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image2374_mask.bmp");
	SpriteGunBullteL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullteL[0].m_x[1] = 75;
	SpriteGunBullteL[0].m_y[1] = 276;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image2376.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image2376_mask.bmp");
	SpriteGunBullteL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullteL[0].m_x[2] = 72;
	SpriteGunBullteL[0].m_y[2] = 276;
	SpriteGunBullteL[0].Init(80, 271, 1, 50, timeGetTime(), 3, false, SpriteMode::PAUSE, ImageMode::DEFFIRENT);

	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1397.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1397_mask.bmp");
	SpritePlayerUpRUpNorlmal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1399.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1399_mask.bmp");
	SpritePlayerUpRUpNorlmal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1401.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1401_mask.bmp");
	SpritePlayerUpRUpNorlmal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRUpNorlmal.Init(112, 258, 1, 50, timeGetTime(), 3, false, SpriteMode::ONCEL);

	//向上开枪的图画
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1607.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1607_mask.bmp");
	SpritePlayerUpRUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRUpgun.m_x[0] = 110;
	SpritePlayerUpRUpgun.m_y[0] = 231;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1609.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1609_mask.bmp");
	SpritePlayerUpRUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRUpgun.m_x[1] = 110;
	SpritePlayerUpRUpgun.m_y[1] = 221;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1611.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1611_mask.bmp");
	SpritePlayerUpRUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRUpgun.m_x[2] = 110;
	SpritePlayerUpRUpgun.m_y[2] = 221;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1613.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1613_mask.bmp");
	SpritePlayerUpRUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRUpgun.m_x[3] = 110;
	SpritePlayerUpRUpgun.m_y[3] = 225;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1615.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1615_mask.bmp");
	SpritePlayerUpRUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRUpgun.m_x[4] = 106;
	SpritePlayerUpRUpgun.m_y[4] = 231;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1617.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1617_mask.bmp");
	SpritePlayerUpRUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpRUpgun.m_x[5] = 106;
	SpritePlayerUpRUpgun.m_y[5] = 225;
	SpritePlayerUpRUpgun.Init(112, 238, 1, 40, timeGetTime(), 6, false, SpriteMode::PAUSE,ImageMode::DEFFIRENT);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1397.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1397_mask.bmp");
	SpritePlayerUpLUpNorlmal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpNorlmal.m_x[0] = 106;
	SpritePlayerUpLUpNorlmal.m_y[0] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1399.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1399_mask.bmp");
	SpritePlayerUpLUpNorlmal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpNorlmal.m_x[1] = 112;
	SpritePlayerUpLUpNorlmal.m_y[1] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1401.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1401_mask.bmp");
	SpritePlayerUpLUpNorlmal.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpNorlmal.m_x[2] = 112;
	SpritePlayerUpLUpNorlmal.m_y[2] = 258;
	SpritePlayerUpLUpNorlmal.Init(112, 258, 1, 50, timeGetTime(), 3, false, SpriteMode::ONCEL,ImageMode::DEFFIRENT);

	//向上开枪的图画
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1607.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1607_mask.bmp");
	SpritePlayerUpLUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpgun.m_x[0] = 126;
	SpritePlayerUpLUpgun.m_y[0] = 231;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1609.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1609_mask.bmp");
	SpritePlayerUpLUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpgun.m_x[1] = 131;
	SpritePlayerUpLUpgun.m_y[1] = 221;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1611.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1611_mask.bmp");
	SpritePlayerUpLUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpgun.m_x[2] = 130;
	SpritePlayerUpLUpgun.m_y[2] = 221;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1613.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1613_mask.bmp");
	SpritePlayerUpLUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpgun.m_x[3] = 130;
	SpritePlayerUpLUpgun.m_y[3] = 225;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1615.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1615_mask.bmp");
	SpritePlayerUpLUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpgun.m_x[4] = 131;
	SpritePlayerUpLUpgun.m_y[4] = 231;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1617.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1617_mask.bmp");
	SpritePlayerUpLUpgun.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerUpLUpgun.m_x[5] = 128;
	SpritePlayerUpLUpgun.m_y[5] = 225;
	SpritePlayerUpLUpgun.Init(112, 238, 1, 40, timeGetTime(), 6, false, SpriteMode::PAUSE, ImageMode::DEFFIRENT);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//加载子弹向上的
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image2372 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image2372_mask - 副本.bmp");
	SpriteGunBullteU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullteU[0].m_x[0] = 129;
	SpriteGunBullteU[0].m_y[0] = 226;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image2374 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image2374_mask - 副本.bmp");
	SpriteGunBullteU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullteU[0].m_x[1] = 133;
	SpriteGunBullteU[0].m_y[1] = 222;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image2376 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image2376_mask - 副本.bmp");
	SpriteGunBullteU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteGunBullteU[0].m_x[2] = 133;
	SpriteGunBullteU[0].m_y[2] = 219;
	SpriteGunBullteU[0].Init(185, 271, 1, 50, timeGetTime(), 3, false, SpriteMode::PAUSE, ImageMode::DEFFIRENT);


	//加载扔手雷的人物图(右)
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1717.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1717_mask.bmp");
	SpritePlayerShoulei.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1719.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1719_mask.bmp");
	SpritePlayerShoulei.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1721.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1721_mask.bmp");
	SpritePlayerShoulei.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1723.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1723_mask.bmp");
	SpritePlayerShoulei.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1725.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1725_mask.bmp");
	SpritePlayerShoulei.Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image1726.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image1726_mask.bmp");
	SpritePlayerShoulei.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerShoulei.Init(112, 258, 1, 60, timeGetTime(), 6, false, SpriteMode::PAUSE);

	//加载扔手雷的人物图(左)
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1717.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1717_mask.bmp");
	SpritePlayerShouleiL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerShouleiL.m_x[0] = 93;
	SpritePlayerShouleiL.m_y[0] = 259;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1719.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1719_mask.bmp");
	SpritePlayerShouleiL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerShouleiL.m_x[1] = 86;
	SpritePlayerShouleiL.m_y[1] = 259;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1721.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1721_mask.bmp");
	SpritePlayerShouleiL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerShouleiL.m_x[2] = 86;
	SpritePlayerShouleiL.m_y[2] = 259;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1723.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1723_mask.bmp");
	SpritePlayerShouleiL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerShouleiL.m_x[3] = 86;
	SpritePlayerShouleiL.m_y[3] = 259;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1725.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1725_mask.bmp");
	SpritePlayerShouleiL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerShouleiL.m_x[4] = 86;
	SpritePlayerShouleiL.m_y[4] = 259;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image1726.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image1726_mask.bmp");
	SpritePlayerShouleiL.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlayerShouleiL.m_x[5] = 86;
	SpritePlayerShouleiL.m_y[5] = 259;
	SpritePlayerShouleiL.Init(86, 259, 1, 60, timeGetTime(), 6, false, SpriteMode::PAUSE,ImageMode::DEFFIRENT);

	//加载手雷(右)
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image377.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image377_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[0] = 160;
	SpriteShoulei[0].m_y[0] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3771.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3771_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[1] = 205;
	SpriteShoulei[0].m_y[1] = 250;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3772.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3772_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[2] = 210;
	SpriteShoulei[0].m_y[2] = 246;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3773.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3773_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[3] = 225;
	SpriteShoulei[0].m_y[3] = 251;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3774.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3774_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[4] = 230;
	SpriteShoulei[0].m_y[4] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3775.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3775_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[5] = 235;
	SpriteShoulei[0].m_y[5] = 283;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3776.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3776_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[6] = 240;
	SpriteShoulei[0].m_y[6] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3777.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3777_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[7] = 250;
	SpriteShoulei[0].m_y[7] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3778.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3778_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[8] = 252;
	SpriteShoulei[0].m_y[8] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\image3779.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\image3779_mask.bmp");
	SpriteShoulei[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShoulei[0].m_x[9] = 255;
	SpriteShoulei[0].m_y[9] = 300;
	SpriteShoulei[0].Init(120, 253, 1,40, timeGetTime(), 7, false, SpriteMode::PAUSE, ImageMode::DEFFIRENT);
	//加载向左的手雷 
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image377.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image377_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[0] = 80;
	SpriteShouleiL[0].m_y[0] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3771.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3771_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[1] = 35;
	SpriteShouleiL[0].m_y[1] = 250;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3772.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3772_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[2] = 30;
	SpriteShouleiL[0].m_y[2] = 246;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3773.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3773_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[3] = 15;
	SpriteShouleiL[0].m_y[3] = 251;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3774.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3774_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[4] = 10;
	SpriteShouleiL[0].m_y[4] = 258;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3775.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3775_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[5] = 5;
	SpriteShouleiL[0].m_y[5] = 283;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3776.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3776_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[6] = 0;
	SpriteShouleiL[0].m_y[6] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3777.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3777_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[7] = -12;
	SpriteShouleiL[0].m_y[7] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3778.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3778_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[8] = -14;
	SpriteShouleiL[0].m_y[8] = 300;
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\image3779.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\image3779_mask.bmp");
	SpriteShouleiL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiL[0].m_x[9] = -15;
	SpriteShouleiL[0].m_y[9] = 300;
	SpriteShouleiL[0].Init(120, 253, 1, 40, timeGetTime(), 7, false, SpriteMode::PAUSE, ImageMode::DEFFIRENT);

	//记载手雷爆炸的资源
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image20.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image20_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image22.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image22_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image24.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image24_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image26.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image26_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image28.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image28_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image30.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image30_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image32.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image32_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image34.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image34_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image36.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image36_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image38.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image38_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image40.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image40_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image42.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image42_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image44.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image44_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image46.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image46_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image48.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image48_mask.bmp");
	SpriteShouleiExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteShouleiExplode[0].Init(300, 155, 1, 100, timeGetTime(), 15, false, SpriteMode::PAUSE);


	//人质
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2242.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2242_mask.bmp");
	SpriteHostage[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2244.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2244_mask.bmp");
	SpriteHostage[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2246.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2246_mask.bmp");
	SpriteHostage[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostage[0].Init(300, 270, 1, 500, timeGetTime(), 3, true, SpriteMode::ALLTIME);
	//人质解救动画
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2249.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2249_mask.bmp");
	SpriteHostageRescue[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageRescue[0].m_x[0] = 300;
	SpriteHostageRescue[0].m_y[0] = 265;
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2251.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2251_mask.bmp");
	SpriteHostageRescue[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageRescue[0].m_x[1] = 300;
	SpriteHostageRescue[0].m_y[1] = 255;
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2253.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2253_mask.bmp");
	SpriteHostageRescue[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageRescue[0].m_x[2] = 300;
	SpriteHostageRescue[0].m_y[2] = 253;
	SpriteHostageRescue[0].Init(300, 265, 1, 150, timeGetTime(), 3, false, SpriteMode::PAUSE, ImageMode::DEFFIRENT);

	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2282.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2282_mask.bmp");
	SpriteHostageRun[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2284.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2284_mask.bmp");
	SpriteHostageRun[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2286.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2286_mask.bmp");
	SpriteHostageRun[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2288.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2288_mask.bmp");
	SpriteHostageRun[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2290.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2290_mask.bmp");
	SpriteHostageRun[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2292.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2292_mask.bmp");
	SpriteHostageRun[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2294.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2294_mask.bmp");
	SpriteHostageRun[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageRun[0].Init(300, 240, 1, 80, timeGetTime(), 7, false, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2267.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2267_mask.bmp");
	SpriteHostageGive[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2269.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2269_mask.bmp");
	SpriteHostageGive[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2271.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2271_mask.bmp");
	SpriteHostageGive[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2273.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2273_mask.bmp");
	SpriteHostageGive[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2275.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2275_mask.bmp");
	SpriteHostageGive[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2277.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2277_mask.bmp");
	SpriteHostageGive[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2279.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2279_mask.bmp");
	SpriteHostageGive[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageGive[0].Init(300, 240, 1, 180, timeGetTime(), 7, false, SpriteMode::PAUSE);

	//加载人物掉落的吃的
	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2316.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2316_mask.bmp");
	SpriteHostageGift[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageGift[0].Init(305, 275, 1, 0, 0, 1, false);

	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2326.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2326_mask.bmp");
	SpriteHostageGift[1].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageGift[1].Init(305, 275, 1, 0, 0, 1, false);

	sprintf(CurrentDir, g_CurrentDir, "\\Hostage\\image2324.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\Hostage\\image2324_mask.bmp");
	SpriteHostageGift[2].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteHostageGift[2].Init(305, 280, 1, 0, 0, 1, false);

	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1033.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1033_mask.bmp");
	SpriteSocre0[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre0[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1035.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1035_mask.bmp");
	SpriteSocre1[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre1[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1037.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1037_mask.bmp");
	SpriteSocre2[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre2[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1039.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1039_mask.bmp");
	SpriteSocre3[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre3[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1041.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1041_mask.bmp");
	SpriteSocre4[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre4[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1043.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1043_mask.bmp");
	SpriteSocre5[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre5[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1045.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1045_mask.bmp");
	SpriteSocre6[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre6[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1047.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1047_mask.bmp");
	SpriteSocre7[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre7[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1049.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1049_mask.bmp");
	SpriteSocre8[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre8[0].Init(0, 0, 0, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\bk\\image1051.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\bk\\image1051_mask.bmp");
	SpriteSocre9[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteSocre9[0].Init(0, 0, 0, 0, 0, 1, false);

	//////////////////////////////////////////////////////////////////////////////////////////////////
	//添加导弹
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image720.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image720_mask.bmp");
	SpriteDDBullte[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteDDBullte[0].Init(190, 271, 1, 0, 0, 1, false, SpriteMode::PAUSE);
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image720.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image720_mask.bmp");
	SpriteDDBullteL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteDDBullteL[0].Init(60, 271, 1, 0, 0, 1, false, SpriteMode::PAUSE);
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image720 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image720_mask - 副本.bmp");
	SpriteDDBullteU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteDDBullteU[0].Init(140, 190, 1, 0, 0, 1, false, SpriteMode::PAUSE);

	//尾焰
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1897.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1897_mask.bmp");
	SpriteDDBullteW[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1899.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1899_mask.bmp");
	SpriteDDBullteW[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1901.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1901_mask.bmp");
	SpriteDDBullteW[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1903.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1903_mask.bmp");
	SpriteDDBullteW[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1905.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1905_mask.bmp");
	SpriteDDBullteW[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1907.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1907_mask.bmp");
	SpriteDDBullteW[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1909.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1909_mask.bmp");
	SpriteDDBullteW[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteDDBullteW[0].Init(140, 257, 1, 200, timeGetTime(), 6, false, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1897.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1897_mask.bmp");
	SpriteDDBullteWL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1899.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1899_mask.bmp");
	SpriteDDBullteWL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1901.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1901_mask.bmp");
	SpriteDDBullteWL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1903.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1903_mask.bmp");
	SpriteDDBullteWL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1905.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1905_mask.bmp");
	SpriteDDBullteWL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1907.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1907_mask.bmp");
	SpriteDDBullteWL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\left\\gun\\image1909.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\left\\gun\\image1909_mask.bmp");
	SpriteDDBullteWL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteDDBullteWL[0].Init(120, 257, 1, 200, timeGetTime(), 6, false, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1897 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1897_mask - 副本.bmp");
	SpriteDDBullteWU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1899 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1899_mask - 副本.bmp");
	SpriteDDBullteWU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1901 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1901_mask - 副本.bmp");
	SpriteDDBullteWU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1903 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1903_mask - 副本.bmp");
	SpriteDDBullteWU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1905 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1905_mask - 副本.bmp");
	SpriteDDBullteWU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1907 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1907_mask - 副本.bmp");
	SpriteDDBullteWU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\player\\right\\gun\\image1909 - 副本.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\player\\right\\gun\\image1909_mask - 副本.bmp");
	SpriteDDBullteWU[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteDDBullteWU[0].Init(125, 271, 1, 200, timeGetTime(), 6, false, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image295.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image295_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image297.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image297_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image299.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image299_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image301.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image301_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image303.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image303_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image305.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image305_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image307.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image307_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image309.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image309_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image311.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image311_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image313.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image313_mask.bmp");
	SpriteDDExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteDDExplode[0].Init(0, 0, 1, 130, timeGetTime(), 9, false, SpriteMode::PAUSE);
	
	//////////////////////////////////////////////////////////////////////////////////////////////////

	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image316.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image316_mask.bmp");
	SpriteOilJar[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJar[0].Init(400, 250, 1, 0, 0, 1);

	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image319.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image319_mask.bmp");
	SpriteOilJarScrap[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarScrap[0].Init(400, 290, 1, 0, 0, 1, false);

	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image325.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image325_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[0] = 380;
	SpriteOilJarExplode[0].m_y[0] = 230;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image327.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image327_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[1] = 380;
	SpriteOilJarExplode[0].m_y[1] = 230;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image329.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image329_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[2] = 380;
	SpriteOilJarExplode[0].m_y[2] = 216;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image331.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image331_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[3] = 380;
	SpriteOilJarExplode[0].m_y[3] = 187;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image333.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image333_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[4] = 380;
	SpriteOilJarExplode[0].m_y[4] = 183;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image335.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image335_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[5] = 380;
	SpriteOilJarExplode[0].m_y[5] = 183;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image337.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image337_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[6] = 380;
	SpriteOilJarExplode[0].m_y[6] = 181;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image339.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image339_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[7] = 380;
	SpriteOilJarExplode[0].m_y[7] = 183;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image341.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image341_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[8] = 380;
	SpriteOilJarExplode[0].m_y[8] = 183;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image343.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image343_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[9] = 380;
	SpriteOilJarExplode[0].m_y[9] = 175;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image345.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image345_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[10] = 380;
	SpriteOilJarExplode[0].m_y[10] = 175;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image347.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image347_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[11] = 380;
	SpriteOilJarExplode[0].m_y[11] = 180;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image349.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image349_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[12] = 380;
	SpriteOilJarExplode[0].m_y[12] = 186;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image351.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image351_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[13] = 380;
	SpriteOilJarExplode[0].m_y[13] = 184;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image353.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image353_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[14] = 380;
	SpriteOilJarExplode[0].m_y[14] = 184;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image355.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image355_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[15] = 380;
	SpriteOilJarExplode[0].m_y[15] = 184;
	sprintf(CurrentDir, g_CurrentDir, "\\explosion\\image357.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\explosion\\image357_mask.bmp");
	SpriteOilJarExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteOilJarExplode[0].m_x[16] = 380;
	SpriteOilJarExplode[0].m_y[16] = 190;
	SpriteOilJarExplode[0].Init(380, 200, 1, 100, timeGetTime(), 17, false, SpriteMode::PAUSE,ImageMode::DEFFIRENT);

	///////////////////////////////////////////////////////////////////////////////////////////////////


	//加载boss一架飞机
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image706.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image706_mask.bmp");
	SpritePlane.Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlane.Init(WIDTH, 0, 1, 0, 0, 1);

	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image711.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image711_mask.bmp");
	SpritePlaneBomb[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlaneBomb[0].Init(WIDTH + 50, 30, 1, 0, 0, 1, false);

	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1929.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1929_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1931.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1931_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1933.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1933_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1935.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1935_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1937.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1937_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1939.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1939_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1941.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1941_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1945.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1945_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1947.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1947_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1949.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1949_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1951.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1951_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1953.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1953_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1955.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1955_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1957.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1957_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1959.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1959_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1961.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1961_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1963.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1963_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1965.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1965_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1967.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1967_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\plane\\image1969.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\plane\\image1969_mask.bmp");
	SpritePlaneBombExplode[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpritePlaneBombExplode[0].Init(0, 0, 1, 100, timeGetTime(), 20, false, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\image1236.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\image1236_mask.bmp");
	SpriteEnmyBullet[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyBullet[0].Init(0, 0, 1, 0, 0, 1, false);
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\image1266.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\image1266_mask.bmp");
	SpriteEnmyBulletP[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyBulletP[0].Init(0, 0, 1, 0, 0, 1, false);

	SpriteEnmyBulletL[0]= SpriteEnmyBullet[0];
	SpriteEnmyBulletPL[0]= SpriteEnmyBulletP[0];
	
	//拷贝所有水果
	for (int i = 3; i < NUMHOSTAGE; i++)
		SpriteHostageGift[i] = SpriteHostageGift[rand()%3];

	//拷贝所有的子弹，手雷,人质
	for (int i = 1; i < 50; i++)
	{
		SpriteGunBullte[i] = SpriteGunBullte[0];
		SpriteGunBullteL[i] = SpriteGunBullteL[0];
		SpriteGunBullteU[i] = SpriteGunBullteU[0];
		SpriteDDBullte[i] = SpriteDDBullte[0];
		SpriteDDBullteL[i] = SpriteDDBullteL[0];
		SpriteDDBullteU[i] = SpriteDDBullteU[0];
		SpriteDDBullteW[i] = SpriteDDBullteW[0];
		SpriteDDBullteWL[i] = SpriteDDBullteWL[0];
		SpriteDDBullteWU[i] = SpriteDDBullteWU[0];
		SpriteDDExplode[i] = SpriteDDExplode[0];
		SpritePlaneBomb[i] = SpritePlaneBomb[0];
		SpritePlaneBombExplode[i] = SpritePlaneBombExplode[0];
		SpriteEnmyBullet[i] = SpriteEnmyBullet[0];
		SpriteEnmyBulletP[i] = SpriteEnmyBulletP[0];
		SpriteEnmyBulletL[i] = SpriteEnmyBullet[0];
		SpriteEnmyBulletPL[i] = SpriteEnmyBulletP[0];
		
		if (i < 10)
		{
			SpriteShoulei[i] = SpriteShoulei[0];
			SpriteShouleiL[i] = SpriteShouleiL[0];
			SpriteShouleiExplode[i]= SpriteShouleiExplode[0];
			

		}
		if (i < 13)
		{
			//分数的具体实现就是在所有位置都有10个数，通过显示隐藏来控制分数的显示
			SpriteSocre0[i] = SpriteSocre0[0];
			SpriteSocre1[i] = SpriteSocre1[0];
			SpriteSocre2[i] = SpriteSocre2[0];
			SpriteSocre3[i] = SpriteSocre3[0];
			SpriteSocre4[i] = SpriteSocre4[0];
			SpriteSocre5[i] = SpriteSocre5[0];
			SpriteSocre6[i] = SpriteSocre6[0];
			SpriteSocre7[i] = SpriteSocre7[0];
			SpriteSocre8[i] = SpriteSocre8[0];
			SpriteSocre9[i] = SpriteSocre9[0];
			SpriteSocre0[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre1[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre2[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre3[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre4[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre5[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre6[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre7[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre8[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();
			SpriteSocre9[i].m_x[0] = i * SpriteSocre0[0].images.at(0)->image->getwidth();

		}
		if (i < NUMHOSTAGE)
		{
			//对人质位置的随机初始化
			int randnum = rand() % (Scene1.m_bkimages.size()*WIDTH-500);
			SpriteHostage[i] = SpriteHostage[0];
			SpriteHostage[i].addXY(randnum, 0);

			SpriteHostageRescue[i] = SpriteHostageRescue[0];
			SpriteHostageRescue[i].addXY(randnum, 0);

			SpriteHostageRun[i] = SpriteHostageRun[0];
			SpriteHostageRun[i].addXY(randnum, 0);

			SpriteHostageGive[i] = SpriteHostageGive[0];
			SpriteHostageGive[i].addXY(randnum, 0);

			SpriteHostageGift[i].addXY(randnum, 0);

			randnum = rand() % (Scene1.m_bkimages.size()*WIDTH - 500);
			SpriteOilJar[i] = SpriteOilJar[0];
			SpriteOilJar[i].addXY(randnum, 0);

			SpriteOilJarExplode[i] = SpriteOilJarExplode[0];
			SpriteOilJarExplode[i].addXY(randnum, 0);

			SpriteOilJarScrap[i] = SpriteOilJarScrap[0];
			SpriteOilJarScrap[i].addXY(randnum, 0);
		}
			
	}
	////加载敌军的动画
	//第一种兵种
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image491.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image491_mask.bmp");
	SpriteEnmy[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image493.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image493_mask.bmp");
	SpriteEnmy[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image495.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image495_mask.bmp");
	SpriteEnmy[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image497.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image497_mask.bmp");
	SpriteEnmy[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image499.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image499_mask.bmp");
	SpriteEnmy[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image547.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image547_mask.bmp");
	SpriteEnmy[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image549.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image549_mask.bmp");
	SpriteEnmy[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmy[0].Init(300, 257, 1, 100, 0, 7, false, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image491.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image491_mask.bmp");
	SpriteEnmyL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image493.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image493_mask.bmp");
	SpriteEnmyL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image495.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image495_mask.bmp");
	SpriteEnmyL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image497.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image497_mask.bmp");
	SpriteEnmyL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image499.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image499_mask.bmp");
	SpriteEnmyL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image547.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image547_mask.bmp");
	SpriteEnmyL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image549.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image549_mask.bmp");
	SpriteEnmyL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyL[0].Init(300, 257, 1, 100, 0, 7, true,SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image502.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image502_mask.bmp");
	SpriteEnmyP[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image504.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image504_mask.bmp");
	SpriteEnmyP[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image506.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image506_mask.bmp");
	SpriteEnmyP[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image508.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image508_mask.bmp");
	SpriteEnmyP[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image510.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image510_mask.bmp");
	SpriteEnmyP[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image570.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image570_mask.bmp");
	SpriteEnmyP[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyP[0].Init(200, 257, 1, 100, 0, 6, false, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image502.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image502_mask.bmp");
	SpriteEnmyPL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image504.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image504_mask.bmp");
	SpriteEnmyPL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image506.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image506_mask.bmp");
	SpriteEnmyPL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image508.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image508_mask.bmp");
	SpriteEnmyPL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image510.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image510_mask.bmp");
	SpriteEnmyPL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image570.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image570_mask.bmp");
	SpriteEnmyPL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyPL[0].Init(200, 257, 1, 100, 0, 6, true, SpriteMode::PAUSE);

	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image658.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image658_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[0] = 0;
	SpriteEnmyDead[0].m_y[0] = 257;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image660.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image660_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[1] = 0;
	SpriteEnmyDead[0].m_y[1] = 262;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image662.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image662_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[2] = 0;
	SpriteEnmyDead[0].m_y[2] = 262;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image664.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image664_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[3] = 0;
	SpriteEnmyDead[0].m_y[3] = 265;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image666.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image666_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[4] = 0;
	SpriteEnmyDead[0].m_y[4] = 274;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image668.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image668_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[5] = 0;
	SpriteEnmyDead[0].m_y[5] = 285;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image670.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image670_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[6] = 0;
	SpriteEnmyDead[0].m_y[6] = 294;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image672.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image672_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[7] = 0;
	SpriteEnmyDead[0].m_y[7] = 295;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image674.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image674_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[8] = 0;
	SpriteEnmyDead[0].m_y[8] = 297;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\right\\image676.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\right\\image676_mask.bmp");
	SpriteEnmyDead[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDead[0].m_x[9] = 0;
	SpriteEnmyDead[0].m_y[9] = 297;
	SpriteEnmyDead[0].Init(0, 0, 1, 100, 0, 10, false,SpriteMode::PAUSE,ImageMode::DEFFIRENT);

	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image658.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image658_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[0] = 0;
	SpriteEnmyDeadL[0].m_y[0] = 257;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image660.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image660_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[1] = 0;
	SpriteEnmyDeadL[0].m_y[1] = 259;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image662.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image662_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[2] = 0;
	SpriteEnmyDeadL[0].m_y[2] = 262;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image664.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image664_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[3] = 0;
	SpriteEnmyDeadL[0].m_y[3] = 262;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image666.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image666_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[4] = 0;
	SpriteEnmyDeadL[0].m_y[4] = 265;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image668.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image668_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[5] = 0;
	SpriteEnmyDeadL[0].m_y[5] = 274;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image670.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image670_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[6] = 0;
	SpriteEnmyDeadL[0].m_y[6] = 285;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image672.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image672_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[7] = 0;
	SpriteEnmyDeadL[0].m_y[7] = 294;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image674.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image674_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[8] = 0;
	SpriteEnmyDeadL[0].m_y[8] = 295;
	sprintf(CurrentDir, g_CurrentDir, "\\enmys\\left\\image676.bmp");
	sprintf(CurrentDir1, g_CurrentDir, "\\enmys\\left\\image676_mask.bmp");
	SpriteEnmyDeadL[0].Add(LoadImageElement(CurrentDir, CurrentDir1));
	SpriteEnmyDeadL[0].m_x[9] = 0;
	SpriteEnmyDeadL[0].m_y[9] = 297;
	SpriteEnmyDeadL[0].Init(0, 0, 1, 100, 0, 10, false,SpriteMode::PAUSE, ImageMode::DEFFIRENT);

	SpriteEnmyDeadP[0] = SpriteEnmyDead[0];
	SpriteEnmyDeadPL[0]= SpriteEnmyDeadL[0];

	for (int i = 1; i < NUMENMY; i++)
	{
		int randnum = rand() % (Scene1.m_bkimages.size()*WIDTH - 500);
		SpriteEnmy[i] = SpriteEnmy[0];
		SpriteEnmy[i].addXY(randnum,0);
		SpriteEnmyL[i] = SpriteEnmyL[0];
		SpriteEnmyL[i].addXY(randnum, 0);

		randnum = rand() % (Scene1.m_bkimages.size()*WIDTH - 500);
		SpriteEnmyP[i] = SpriteEnmyP[0];
		SpriteEnmyP[i].addXY(randnum, 0);
		SpriteEnmyPL[i] = SpriteEnmyPL[0];
		SpriteEnmyPL[i].addXY(randnum, 0);

		SpriteEnmyDead[i] = SpriteEnmyDead[0];
		SpriteEnmyDeadL[i] = SpriteEnmyDeadL[0];
		SpriteEnmyDeadP[i] = SpriteEnmyDeadP[0];
		SpriteEnmyDeadPL[i] = SpriteEnmyDeadPL[0];
	}
	//初始化用于玩家血量的矩形
	g_PlayerBlood.left = 0;;
	g_PlayerBlood.top = SpriteSocre0[0].images.at(0)->image->getheight();
	g_PlayerBlood.right = 200;
	g_PlayerBlood.bottom = SpriteSocre0[0].images.at(0)->image->getheight()+ 12;

	g_PlayerBloodSolid.left = 0;;
	g_PlayerBloodSolid.top = SpriteSocre0[0].images.at(0)->image->getheight();
	g_PlayerBloodSolid.right = 200;
	g_PlayerBloodSolid.bottom = SpriteSocre0[0].images.at(0)->image->getheight()+ 12;

	//初始化boss的血条
	g_rePlaneBlood.left = 260;
	g_rePlaneBlood.top=HIGH-12;
	g_rePlaneBlood.right = WIDTH;
	g_rePlaneBlood.bottom=HIGH;

	g_rePlaneBloodSolid.left = 260;
	g_rePlaneBloodSolid.top =HIGH-12;
	g_rePlaneBloodSolid.right = WIDTH;
	g_rePlaneBloodSolid.bottom = HIGH;
	
	Musicflgs = Musicflg::ALLSTART1;
}

void CloseScene1()//关闭场景2
{
	CloseScene1Music();
	Scene1.DeleteAll();
}
void CloseScene1Music()//关闭场景2音乐
{
	Musicflgs = Musicflg::CLOSE1;
}
void CloseScene0Music()//关闭场景1音乐
{
	//mciSendString("close bkstart", NULL, 0, NULL);
	Musicflgs = Musicflg::CLOSE0;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//封装人物动作的函数
void PlayerForward()
{
	if ((SpritePlayerDownR.m_x[0] > (WIDTH / 2)) && (-Scene1.m_bkx < (WIDTH*(Scene1.m_bkimages.size() - 1))))
	{
		//在人物移动到场景一定位置时移动场景
		Scene1.m_bkx -= 5;//移动背景
		for (int i = 0; i<50; i++)
		{
			SpriteEnmyBullet[i].m_x[0] -= 5;
			SpriteEnmyBulletL[i].m_x[0] -= 5;
			SpriteEnmyBulletP[i].m_x[0] -= 5;
			SpriteEnmyBulletPL[i].m_x[0] -= 5;
			if (i < NUMENMY)
			{
				SpriteEnmy[i].m_x[0] -= 5;
				SpriteEnmyL[i].m_x[0] -= 5;
				SpriteEnmyP[i].m_x[0] -= 5;
				SpriteEnmyPL[i].m_x[0] -= 5;
				if (SpriteEnmyDead[i].m_visible)
					SpriteEnmyDead[i].reduceXY(5, 0);
				if (SpriteEnmyDeadL[i].m_visible)
					SpriteEnmyDeadL[i].reduceXY(5, 0);
				if (SpriteEnmyDeadP[i].m_visible)
					SpriteEnmyDeadP[i].reduceXY(5, 0);
				if (SpriteEnmyDeadPL[i].m_visible)
					SpriteEnmyDeadPL[i].reduceXY(5, 0);
			}
			if (i < 10)
			{
				if (SpriteShouleiExplode[i].m_visible == true)
				{
					SpriteShouleiExplode[i].addXY(-5, 0);//爆炸的位置也要跟随场景移动
				}
			}
			if (i < NUMHOSTAGE)
			{
				SpriteHostage[i].addXY(-5, 0);//同样一些需要跟随场景一起的动画之类的
				SpriteHostageRescue[i].addXY(-5, 0);
				SpriteHostageRun[i].addXY(-5, 0);
				SpriteHostageGive[i].addXY(-5, 0);
				SpriteHostageGift[i].addXY(-5, 0);
				SpriteOilJar[i].addXY(-5, 0);
				SpriteOilJarExplode[i].addXY(-5, 0);
				SpriteOilJarScrap[i].addXY(-5, 0);
			}
			if (SpriteDDExplode[i].m_visible == true)
			{
				SpriteDDExplode[i].addXY(-5, 0);
			}
		}
	}
	else
	{
		if (SpritePlayerDownR.m_x[0] <= WIDTH-60)//防止人物超出屏幕
		{
			SpritePlayerDownR.addXY(5, 0);//移动人物相关的动画
			SpritePlayerUpRHorNormal.addXY(5, 0);
			SpritePlayerRunR.addXY(5, 0);
			SpritePlayerUpRHorgun.addXY(5, 0);
			SpritePlayerUpRUpgun.addXY(5, 0);
			SpritePlayerUpRUpNorlmal.addXY(5,0);
			SpritePlayerUpLUpgun.addXY(5, 0);
			SpritePlayerUpLUpNorlmal.addXY(5, 0);
			SpritePlayerShoulei.addXY(5, 0);
			SpritePlayerShouleiL.addXY(5, 0);
			

			SpritePlayerDownL.addXY(5, 0);
			SpritePlayerUpLHorNormal.addXY(5, 0);
			SpritePlayerRunL.addXY(5, 0);
			SpritePlayerUpLHorgun.addXY(5, 0);
			for (int i = 0; i < 50; i++)
			{
				/*if (Weapons == Weapon::GUN)//人物移动，是未显示的子弹也移动
				{*/
					if (SpriteGunBullte[i].m_visible == false)
						SpriteGunBullte[i].addXY(5, 0);
					if (SpriteGunBullteL[i].m_visible == false)
						SpriteGunBullteL[i].addXY(5, 0);
					if (SpriteGunBullteU[i].m_visible == false)
						SpriteGunBullteU[i].addXY(5, 0);
			/*	}
				else if (Weapons == Weapon::DD)
				{*/
					if (SpriteDDBullte[i].m_visible == false)
						SpriteDDBullte[i].addXY(5, 0);
					if (SpriteDDBullteL[i].m_visible == false)
						SpriteDDBullteL[i].addXY(5, 0);
					if (SpriteDDBullteU[i].m_visible == false)
						SpriteDDBullteU[i].addXY(5, 0);
					if (SpriteDDBullteW[i].m_visible == false)
						SpriteDDBullteW[i].addXY(5, 0);
					if (SpriteDDBullteWL[i].m_visible == false)
						SpriteDDBullteWL[i].addXY(5, 0);
					if (SpriteDDBullteWU[i].m_visible == false)
						SpriteDDBullteWU[i].addXY(5, 0);
			//	}
				


				if (i < 10)//移动手雷
				{
					if (SpriteShoulei[i].m_visible == false)
						SpriteShoulei[i].addXY(5, 0);
					if (SpriteShouleiL[i].m_visible == false)
						SpriteShouleiL[i].addXY(5, 0);
				}
				
			}
		}
		
	}
	//各种动画组合的显示，成人物的最终图形
	if(SpritePlayerUpRHorgun.m_visible!=true)
		SpritePlayerRunR.m_visible = true;
	SpritePlayerDownR.m_visible = true;
	SpritePlayerUpRHorNormal.m_visible = false;
	SpritePlayerUpRUpNorlmal.m_visible = false;
	SpritePlayerShoulei.m_visible = false;
	SpritePlayerShouleiL.m_visible = false;
	SpritePlayerDownR.m_mode = SpriteMode::ALLTIME;
	SpritePlayerRunR.m_mode = SpriteMode::ALLTIME;
	
}
void PlayerBack()
{
	if (SpritePlayerDownL.m_x[0] > 18)//与上一个函数差不多
	{
		SpritePlayerDownL.addXY(-5, 0);
		SpritePlayerUpLHorNormal.addXY(-5, 0);
		SpritePlayerRunL.addXY(-5, 0);
		SpritePlayerUpLHorgun.addXY(-5, 0);
		SpritePlayerUpRUpNorlmal.addXY(-5, 0);
		SpritePlayerUpRUpgun.addXY(-5, 0);
		SpritePlayerUpLUpNorlmal.addXY(-5, 0);
		SpritePlayerUpLUpgun.addXY(-5, 0);
		SpritePlayerShoulei.addXY(-5, 0);
		SpritePlayerShouleiL.addXY(-5, 0);

		SpritePlayerDownR.addXY(-5, 0);
		SpritePlayerUpRHorNormal.addXY(-5, 0);
		SpritePlayerRunR.addXY(-5, 0);
		SpritePlayerUpRHorgun.addXY(-5, 0);
		for (int i = 0; i < 50; i++)
		{
			/*if (Weapons == Weapon::GUN)
			{*///移动随身弹药
				if (SpriteGunBullteL[i].m_visible == false)
					SpriteGunBullteL[i].addXY(-5, 0);
				if (SpriteGunBullte[i].m_visible == false)
					SpriteGunBullte[i].addXY(-5, 0);
				if (SpriteGunBullteU[i].m_visible == false)
					SpriteGunBullteU[i].addXY(-5, 0);
		/*	}
			else if (Weapons == Weapon::DD)
			{*/
				if (SpriteDDBullteL[i].m_visible == false)
					SpriteDDBullteL[i].addXY(-5, 0);
				if (SpriteDDBullte[i].m_visible == false)
					SpriteDDBullte[i].addXY(-5, 0);
				if (SpriteDDBullteU[i].m_visible == false)
					SpriteDDBullteU[i].addXY(-5, 0);
				if (SpriteDDBullteWL[i].m_visible == false)
					SpriteDDBullteWL[i].addXY(-5, 0);
				if (SpriteDDBullteW[i].m_visible == false)
					SpriteDDBullteW[i].addXY(-5, 0);
				if (SpriteDDBullteWU[i].m_visible == false)
					SpriteDDBullteWU[i].addXY(-5, 0);
			//}
			
			if (i < 10)
			{
				if (SpriteShoulei[i].m_visible == false)
					SpriteShoulei[i].addXY(-5, 0);
				if (SpriteShouleiL[i].m_visible == false)
					SpriteShouleiL[i].addXY(-5, 0);
			}
			
		}
	}
	if (SpritePlayerUpLHorgun.m_visible != true)//各种显示组合
		SpritePlayerRunL.m_visible = true;
	SpritePlayerDownL.m_visible = true;
	SpritePlayerUpLHorNormal.m_visible = false;
	SpritePlayerShoulei.m_visible = false;
	SpritePlayerShouleiL.m_visible = false;
	SpritePlayerDownL.m_mode = SpriteMode::ALLTIME;
	SpritePlayerRunL.m_mode = SpriteMode::ALLTIME;

}
void PlayerJump(int nDirection)
{
	g_GJump = 18;
	isJump = true;
	/*SpritePlayerDownL.reduceXY(0,)
	SpritePlayerDownR;
	SpritePlayerUpLUpNorlmal, SpritePlayerUpLHorNormal;//玩家的动画
	SpritePlayerUpRUpNorlmal, SpritePlayerUpRHorNormal;//玩家的动画
	SpritePlayerUpLUpgun, SpritePlayerUpLHorgun;//玩家的动画
	SpritePlayerUpRUpgun, SpritePlayerUpRHorgun;//玩家的动画
	SpritePlayerRunR, SpritePlayerRunL;*/
}
void PlayerShoot(int nDiraction)
{
	if (nDiraction == 1)
	{
		if (timeGetTime() - g_bulletstarttime >= g_bulletinteral)
		{
			/*int *nflg = new int;//不知为何放在这里会有一丝卡顿，尽管使用多线程
			*nflg=Musicflg::GUNSHOOT;
			g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
			SpritePlayerUpRUpgun.m_visible = false;
			SpritePlayerUpRHorNormal.m_visible = false;
			SpritePlayerRunR.m_visible = false;
			SpritePlayerUpRHorgun.m_visible = true;
			if (Weapons == Weapon::GUN)//根据武器标志来播放相应的子弹
			{
				SpriteGunBullte[g_NumBullte].m_visible = true;
				SpriteGunBullte[g_NumBullte].m_mode = SpriteMode::ONCEL;
			}
			else if (Weapons == Weapon::DD)
			{
				SpriteDDBullte[g_NumBullte].m_visible = true;
				SpriteDDBullte[g_NumBullte].m_mode = SpriteMode::ONCEL;
				SpriteDDBullteW[g_NumBullte].m_visible = true;
				SpriteDDBullteW[g_NumBullte].m_mode = SpriteMode::ONCEFADE;
			}
			SpritePlayerUpRHorgun.m_mode = SpriteMode::ONCE;
			g_NumBullte++;
			g_bulletstarttime = timeGetTime();
		}
	}
	else if (nDiraction == 2)
	{
		if (timeGetTime() - g_bulletstarttimeL >= g_bulletinteralL)
		{
			/*int *nflg = new int;
			*nflg = Musicflg::GUNSHOOT;
			g_GameEngine.GetCurrentScene()->m_scenemusic(nflg);//播放枪击音乐*/
			SpritePlayerUpLUpgun.m_visible = false;
			SpritePlayerUpLHorNormal.m_visible = false;
			SpritePlayerRunL.m_visible = false;
			SpritePlayerUpLHorgun.m_visible = true;
			if (Weapons == Weapon::GUN)//同上根据武器标志来播放相应的子弹
			{
				SpriteGunBullteL[g_NumBullteL].m_visible = true;
				SpriteGunBullteL[g_NumBullteL].m_mode = SpriteMode::ONCEL;
			}
			else if (Weapons == Weapon::DD)
			{
				SpriteDDBullteL[g_NumBullteL].m_visible = true;
				SpriteDDBullteL[g_NumBullteL].m_mode = SpriteMode::ONCEL;
				SpriteDDBullteWL[g_NumBullteL].m_visible = true;
				SpriteDDBullteWL[g_NumBullteL].m_mode = SpriteMode::ONCEFADE;
			}
			SpritePlayerUpLHorgun.m_mode = SpriteMode::ONCE;
			g_NumBullteL++;
			g_bulletstarttimeL = timeGetTime();
		}
	}
	
	
}
void PlayerDead(int nDirection)
{

}

void PlayerDefalut(int nDiraction)
{
	//任何键都不按下时的默认状态
	if (nDiraction == 1)
	{
		//默认的人物动画组合右方的
		SpritePlayerDownR.Pause(0);
		SpritePlayerRunR.Pause(0);
		SpritePlayerDownR.m_visible = true;
		SpritePlayerUpRHorNormal.m_visible = true;
		SpritePlayerRunR.m_visible = false;
		SpritePlayerUpRHorgun.m_visible = false;
		SpritePlayerUpRHorNormal.m_mode = SpriteMode::ALLTIME;
		SpritePlayerUpRUpNorlmal.Pause(0);
		SpritePlayerUpRUpNorlmal.m_visible = false;
		SpritePlayerUpRUpgun.m_visible = false;
		SpritePlayerShoulei.m_visible = false;
		SpritePlayerShouleiL.m_visible = false;
		//SpritePlayerUpRUpNorlmal.Pause(0);
	}
	else if(nDiraction==2)
	{
		//默认的人物动画组合左方的
		SpritePlayerDownL.Pause(0);
		SpritePlayerRunL.Pause(0);
		SpritePlayerDownL.m_visible = true;
		SpritePlayerUpLHorNormal.m_visible = true;
		SpritePlayerRunL.m_visible = false;
		SpritePlayerUpLHorgun.m_visible = false;
		SpritePlayerUpLHorNormal.m_mode = SpriteMode::ALLTIME;
		SpritePlayerUpLUpNorlmal.Pause(0);
		SpritePlayerUpLUpNorlmal.m_visible = false;
		SpritePlayerUpLUpgun.m_visible = false;
		SpritePlayerShouleiL.m_visible = false;
		SpritePlayerShoulei.m_visible = false;
	}
	
}

void Scene2Start()
{
	//一个说明的场景，因为后加所以为2
	char filename[260] = { 0 };
	sprintf(filename, g_CurrentDir, "\\bk\\operation-bg.jpg");
	IMAGE* image = new IMAGE;
	loadimage(image, filename);
	Scene2.AddBk(image);

	//相应按钮在图中的位置
	g_Return.left = 436;
	g_Return.top = 343;
	g_Return.right = 517;
	g_Return.bottom = 380;

	g_StartButton.left = 308;
	g_StartButton.top = 272;
	g_StartButton.right = 486;
	g_StartButton.bottom = 313;
}
void CloseScene2()
{

}
void Scene2MouseEvent(MOUSEMSG msg)
{
	POINT pt = { msg.x,msg.y };
	if (msg.uMsg == WM_LBUTTONDOWN)//鼠标左键按下
	{
		if (PtInRect(&g_StartButton, pt))
		{
			MessageBox(g_hWnd, "增加了使用E切换武器\nESC存档", "提示", MB_OK | MB_ICONINFORMATION);
			g_scene = 1;
			g_GameEngine.m_sceneID = g_scene;//切换到场景1
			CloseScene0();//如函数命名
			CloseScene2();
			Scene1Start();
#ifndef _DEBUG
			/*int *nflg = new int;
			*nflg = Musicflg::DEFAULT;
			GameMusic(nflg);*/
#endif // !_DEBUG

		}
		if (PtInRect(&g_Return, pt))
		{
			g_scene = 0;
			g_GameEngine.m_sceneID = g_scene;//切换到场景0
			CloseScene2();

		}
	}
}

void PlayerUp()
{
	//这里是朝向上的一些人物图像组合
	if (g_nDiraction == 1)
	{
		SpritePlayerUpRUpNorlmal.m_visible = true;
		SpritePlayerUpRHorgun.m_visible = false;
		SpritePlayerUpRHorNormal.m_visible = false;
		SpritePlayerUpRUpgun.m_visible = false;
		SpritePlayerRunR.m_visible = false;
		SpritePlayerShoulei.m_visible = false;
		SpritePlayerUpRUpNorlmal.m_mode = SpriteMode::ONCEL;
		
	}
	else if(g_nDiraction==2)
	{
		SpritePlayerUpLUpNorlmal.m_visible = true;
		SpritePlayerUpLHorgun.m_visible = false;
		SpritePlayerUpLHorNormal.m_visible = false;
		SpritePlayerUpLUpgun.m_visible = false;
		SpritePlayerRunL.m_visible = false;
		SpritePlayerShouleiL.m_visible = false;
		SpritePlayerUpLUpNorlmal.m_mode = SpriteMode::ONCEL;
	}
}

void PlayerUpShoot()
{
	//朝上的射击
	if (g_nDiraction == 1)
	{
		SpritePlayerUpRUpNorlmal.m_visible = false;
		SpritePlayerUpRUpgun.m_visible = true;
		SpritePlayerUpRHorgun.m_visible = false;
		SpritePlayerRunR.m_visible = false;
		SpritePlayerUpRUpgun.m_mode = SpriteMode::ONCE;
	}
	else if (g_nDiraction == 2)
	{
		SpritePlayerUpLUpNorlmal.m_visible = false;
		SpritePlayerUpLUpgun.m_visible = true;
		SpritePlayerUpLHorgun.m_visible = false;
		SpritePlayerRunL.m_visible = false;
		SpritePlayerUpLUpgun.m_mode = SpriteMode::ONCE;
	}

	if (timeGetTime() - g_bulletstarttimeU >= g_bulletinteralU)//设置的延迟防止发射过快
	{
		if (Weapons == Weapon::GUN)
		{
			SpriteGunBullteU[g_NumBullteU].m_visible = true;
			SpriteGunBullteU[g_NumBullteU].m_mode = SpriteMode::ONCEL;
		}
		else if (Weapons == Weapon::DD)
		{
			SpriteDDBullteU[g_NumBullteU].m_visible = true;
			SpriteDDBullteU[g_NumBullteU].m_mode = SpriteMode::ONCEL;
			SpriteDDBullteWU[g_NumBullteU].m_visible = true;
			SpriteDDBullteWU[g_NumBullteU].m_mode = SpriteMode::ONCEFADE;
		}
		
		g_NumBullteU++;
		g_bulletstarttimeU = timeGetTime();
	}
}

void PlayerShoulei()
{
	if (g_nDiraction == 1)//右方的
	{
		SpritePlayerShoulei.m_visible = true;//人物扔手雷的组合动画
		SpritePlayerUpRUpNorlmal.m_visible = false;
		SpritePlayerUpRHorNormal.m_visible = false;
		SpritePlayerUpRUpgun.m_visible = false;
		SpritePlayerUpRHorgun.m_visible = false;
		SpritePlayerRunR.m_visible = false;
		SpritePlayerShoulei.m_mode = SpriteMode::ONCE;
		
		/*SpritePlayerUpLUpNorlmal, SpritePlayerUpLHorNormal;//玩家的动画
		SpritePlayerUpRUpNorlmal, SpritePlayerUpRHorNormal;//玩家的动画
		SpritePlayerUpLUpgun, SpritePlayerUpLHorgun;//玩家的动画
		SpritePlayerUpRUpgun, SpritePlayerUpRHorgun;//玩家的动画
		SpritePlayerRunR, SpritePlayerRunL;*/
	}
	else if (g_nDiraction == 2)
	{
		SpritePlayerShouleiL.m_visible = true;//人物扔手雷的组合动画
		SpritePlayerUpLUpNorlmal.m_visible = false;
		SpritePlayerUpLHorNormal.m_visible = false;
		SpritePlayerUpLUpgun.m_visible = false;
		SpritePlayerUpLHorgun.m_visible = false;
		SpritePlayerRunL.m_visible = false;
		SpritePlayerShouleiL.m_mode = SpriteMode::ONCE;
		
	}
	ThrowShoulei();//同时吧手雷扔出去
}

void ThrowShoulei()//手雷的动画
{
	if (g_nDiraction == 1)
	{
		if (timeGetTime() - g_ShouleiStarttime >= g_ShouleiInternal)
		{
			SpriteShoulei[g_ShouleiNum].m_visible = true;
			SpriteShoulei[g_ShouleiNum].m_mode = SpriteMode::ONCEL;
			g_ShouleiNum++;
			g_ShouleiStarttime = timeGetTime();
		}
		
	}
	else if(g_nDiraction==2)
	{
		if (timeGetTime() - g_ShouleiStarttimeL >= g_ShouleiInternalL)
		{
			SpriteShouleiL[g_ShouleiNumL].m_visible = true;
			SpriteShouleiL[g_ShouleiNumL].m_mode = SpriteMode::ONCEL;
			g_ShouleiNumL++;
			g_ShouleiStarttimeL = timeGetTime();
		}
	}
}

void ShouleiBoom(int num,int driection)//爆炸指定的手雷
{
	/*int *nflg = new int;
	*nflg=Musicflg::SHOULEI;
	GameMusic(nflg);*/
	if (driection == 1)
	{
		int tempLen= SpriteShouleiExplode[g_ShouleiExplodeNum].m_x[0] - SpriteShoulei[num].m_x[SpriteShoulei[num].images.size()-1];//相差的距离
		for (int j = 0; j < SpriteShouleiExplode[g_ShouleiExplodeNum].images.size(); j++)
		{
			SpriteShouleiExplode[g_ShouleiExplodeNum].m_x[j] -= (tempLen+20);//对爆炸的位置赋值
		}
		SpriteShoulei[num] = SpriteShoulei[0];//重置手雷
		SpriteShouleiExplode[g_ShouleiExplodeNum].m_visible = true;
		SpriteShouleiExplode[g_ShouleiExplodeNum].m_mode = SpriteMode::ONCEFADE;
	}
	else if (driection == 2)
	{
		int tempLen = SpriteShouleiExplode[g_ShouleiExplodeNum].m_x[0] - SpriteShouleiL[num].m_x[SpriteShouleiL[num].images.size() - 1];//相差的距离
		for (int j = 0; j < SpriteShouleiExplode[g_ShouleiExplodeNum].images.size(); j++)
		{
			SpriteShouleiExplode[g_ShouleiExplodeNum].m_x[j] -= (tempLen+20);//对爆炸的位置赋值
		}
		SpriteShouleiL[num] = SpriteShouleiL[0];//重置手雷
		SpriteShouleiExplode[g_ShouleiExplodeNum].m_visible = true;
		SpriteShouleiExplode[g_ShouleiExplodeNum].m_mode = SpriteMode::ONCEFADE;
	}
	g_ShouleiExplodeNum++;
}

void ShowSocre(long socre)//显示分数
{
	for(int i=12;i>=0;i--)
	{
		SpriteSocre0[i].m_visible = false;//先关闭每个位置的数字
		SpriteSocre1[i].m_visible = false;
		SpriteSocre2[i].m_visible = false;
		SpriteSocre3[i].m_visible = false;
		SpriteSocre4[i].m_visible = false;
		SpriteSocre5[i].m_visible = false;
		SpriteSocre6[i].m_visible = false;
		SpriteSocre7[i].m_visible = false;
		SpriteSocre8[i].m_visible = false;
		SpriteSocre9[i].m_visible = false;
		int anum = socre % 10;
		switch (anum)//然后在按照anum打开
		{
		case 0:
			SpriteSocre0[i].m_visible = true;
			break;
		case 1:
			SpriteSocre1[i].m_visible = true;
			break;
		case 2:
			SpriteSocre2[i].m_visible = true;
			break;
		case 3:
			SpriteSocre3[i].m_visible = true;
			break;
		case 4:
			SpriteSocre4[i].m_visible = true;
			break;
		case 5:
			SpriteSocre5[i].m_visible = true;
			break;
		case 6:
			SpriteSocre6[i].m_visible = true;
			break;
		case 7:
			SpriteSocre7[i].m_visible = true;
			break;
		case 8:
			SpriteSocre8[i].m_visible = true;
			break;
		case 9:
			SpriteSocre9[i].m_visible = true;
			break;
		}
		socre /= 10;
	} 
}

void DDExplode(int num,int x,int y)
{
	/*int *nflg = new int;
	*nflg = Musicflg::SHOULEI;
	GameMusic(nflg);*/
	SpriteDDExplode[num].m_x[0] = x;
	SpriteDDExplode[num].m_y[0] = y;
	SpriteDDExplode[num].m_visible = true;
	SpriteDDExplode[num].m_mode = SpriteMode::ONCEFADE;
}

void PlaneBombExplode(int num)
{
	/*int *pflg = new int;
	*pflg = Musicflg::PLANEBOMB;
	GameMusic(pflg);*/
	Musicflgs = Musicflg::PLANEBOMB;
	SpritePlaneBombExplode[num].m_x[0] = SpritePlaneBomb[num].m_x[0]-50;
	SpritePlaneBombExplode[num].m_y[0] = SpritePlaneBomb[num].m_y[0]-100;
	SpritePlaneBombExplode[num].m_visible = true;
	SpritePlaneBombExplode[num].m_mode = SpriteMode::ONCEFADE;
}

void EnmyDead(int nDirection, int num,bool isp)
{
	Musicflgs = Musicflg::ENMYDEAD;
	if (nDirection == 1)
	{
		if (isp)
		{
			SpriteEnmyDeadP[num].m_visible = true;
			SpriteEnmyDeadP[num].m_mode = ONCEFADE;
			SpriteEnmyDeadP[num].addXY(SpriteEnmyP[num].m_x[0],0);
			//SpriteEnmyDeadP[num].m_y[0] = SpriteEnmyP[num].m_y[0];
		}
		else
		{
			SpriteEnmyDead[num].m_visible = true;
			SpriteEnmyDead[num].m_mode = ONCEFADE;
			SpriteEnmyDead[num].addXY(SpriteEnmy[num].m_x[0], 0);
			//SpriteEnmyDead[num].m_y[0] = SpriteEnmy[num].m_y[0];
		}
		
	}
	else if (nDirection == 2)
	{
		if (isp)
		{
			SpriteEnmyDeadPL[num].m_visible = true;
			SpriteEnmyDeadPL[num].m_mode = ONCEFADE;
			SpriteEnmyDeadPL[num].addXY(SpriteEnmyPL[num].m_x[0],0);
			//SpriteEnmyDeadPL[num].m_y[0] = SpriteEnmyPL[num].m_y[0];
		}
		else
		{
			SpriteEnmyDeadL[num].m_visible = true;
			SpriteEnmyDeadL[num].m_mode = ONCEFADE;
			SpriteEnmyDeadL[num].addXY(SpriteEnmyL[num].m_x[0], 0);
			//SpriteEnmyDeadL[num].m_y[0] = SpriteEnmyL[num].m_y[0];
		}
		
	}
}
void EnmyGunShoot(int nDiraction, bool isp,int x,int y)
{
	if (nDiraction==1)
	{
		if (isp)
		{
			SpriteEnmyBulletP[g_EnmybulletNumP].m_visible = true;
			SpriteEnmyBulletP[g_EnmybulletNumP].m_x[0] = x;
			SpriteEnmyBulletP[g_EnmybulletNumP].m_y[0] = y;
			g_EnmybulletNumP++;
		}
		else
		{
			SpriteEnmyBullet[g_EnmybulletNum].m_visible = true;
			SpriteEnmyBullet[g_EnmybulletNum].m_x[0] = x;
			SpriteEnmyBullet[g_EnmybulletNum].m_y[0] = y;
			g_EnmybulletNum++;
		}
	}
	else if (nDiraction == 2)
	{
		if (isp)
		{
			SpriteEnmyBulletPL[g_EnmybulletNumPL].m_visible = true;
			SpriteEnmyBulletPL[g_EnmybulletNumPL].m_x[0] = x;
			SpriteEnmyBulletPL[g_EnmybulletNumPL].m_y[0] = y;
			g_EnmybulletNumPL++;
		}
		else
		{
			SpriteEnmyBulletL[g_EnmybulletNumL].m_visible = true;
			SpriteEnmyBulletL[g_EnmybulletNumL].m_x[0] = x;
			SpriteEnmyBulletL[g_EnmybulletNumL].m_y[0] = y;
			g_EnmybulletNumL++;
		}
	}
}