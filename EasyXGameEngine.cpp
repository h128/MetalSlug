#include "EasyXGameEngine.h"

int main()
{
	
	/*long g_StartTime=timeGetTime();	//游戏主循环时间间隔控制
	int g_Delay=30;		//设置帧延迟*/
	GameStart();		//游戏初始化
	while (true)		//游戏主循环
	{
		/*进行消息处理*/
	/*	if (_kbhit())
		{
			char msg = _getch();
			KeyEvent(msg);
		}*/
		if (MouseHit())
		{
			MOUSEMSG msg=GetMouseMsg();
			MouseEvent(msg);
		}
		
		long lGameTime = timeGetTime();
		if (lGameTime - EasyxGameEngine::GetStartTime()>=EasyxGameEngine::GetDelay())	//到达下次循环时间
		{
			KeyEvent();//在这里使用GetAsyKeyState解决多个按键同时按下的问题
			GameAction();						//执行游戏逻辑
			EasyxGameEngine::m_starttime = lGameTime;
		}
	}
	GameEnd();
	return 0;
}

void Sprite::Add(ImageElement* imageelement)
{
	images.push_back(imageelement);
}

void Sprite::DeleteAll()
{
	if (!images.empty())//不为空时依次删除
	{
		for (int i = 0; i < images.size(); i++)
		{
			images.at(i)->Delete();
			delete images.at(i);
		}
		images.clear();
	}
	
}

void Sprite::DrawAll()
{
	if (m_index != images.size())//防止用户初始化与实际不符，，自动确定
		m_index = images.size();
	if (m_visible)
	{
		//以下是几种模式的具体实现
		if (timeGetTime() - m_playstarttime >= m_playdelay)//播放延迟
		{
			if (m_imagemode == ImageMode::ALLSAME)
				images.at(m_tempindex)->Draw(m_x[0], m_y[0]);
			else if (m_imagemode == ImageMode::DEFFIRENT)
			{
				images.at(m_tempindex)->Draw(m_x[m_tempindex], m_y[m_tempindex]);
			}
			if (timeGetTime() - m_pausedellaystarttime >= m_pausedelay)
			{
				long CurTime = timeGetTime();
				if (CurTime - m_starttime >= m_delay)
				{
					if (m_mode == SpriteMode::ALLTIME)//一直运动
					{
						m_tempindex++;
						if (m_tempindex >= m_index)
							m_tempindex = 0;
					}
					else if (m_mode == SpriteMode::ONCE)//一次
					{
						m_tempindex++;
						if (m_tempindex >= m_index)
						{
							m_tempindex = 0;
							m_mode = SpriteMode::PAUSE;
						}
					}
					else if (m_mode == SpriteMode::ONCEL)//一次后暂停在最后一帧
					{
						m_tempindex++;
						if (m_tempindex >= m_index)
						{
							m_tempindex -= 1;
							m_mode = SpriteMode::PAUSE;
						}
					}
					else if (m_mode == SpriteMode::ONCEFADE)//一次后自动消失
					{
						m_tempindex++;
						if (m_tempindex >= m_index)
						{
							m_tempindex = 0;
							m_mode = SpriteMode::PAUSE;
							m_visible = false;
						}

					}
					else if (m_mode == SpriteMode::PAUSE)
					{
						m_tempindex = m_tempindex;
					}
					m_starttime = CurTime;
					
				}
				if (m_pausedelay != 0) 
				{
					m_tempindex = 0;
				}
				m_pausedelay = 0;
			}
			m_playdelay = 0;
		}
		
	}
	
}

void Sprite::Init(int x, int y,int z, int delay, int starttime, int index,bool visibel,SpriteMode nMode,ImageMode nImagemode)
{
	if (nImagemode == ImageMode::ALLSAME)
	{
		m_x[0] = x;
		m_y[0] = y;
	}
	m_z = z;
	m_delay = delay;
	m_starttime = starttime;
	m_index = index;
	m_tempindex = 0;
	m_visible = visibel;
	m_mode = nMode;
	m_imagemode = nImagemode;
	m_playdelay = 0;
	m_playstarttime = 0;//timeGetTime();
	m_pausedelay = 0;
	m_pausedellaystarttime = 0;
}

void Sprite::addPauseDelay(int nDelay)
{
	m_pausedellaystarttime = timeGetTime();
	m_pausedelay = nDelay;
	//m_tempindex = reIndex;
}

void Sprite::Pause(int index)//按照帧的索引暂停
{
	m_oringmod = m_mode;
	m_mode = SpriteMode::PAUSE;
	m_tempindex = index;
}
void Sprite::Active()
{
	m_mode = m_oringmod;
}

void Sprite::addXY(int nx, int ny)
{
	if (m_imagemode == ImageMode::ALLSAME)
	{
		m_x[0] += nx;
		m_y[0] += ny;
	}
	else if (m_imagemode == ImageMode::DEFFIRENT)
	{
		for (int i = 0; i < m_index; i++)
		{
			m_x[i] += nx;
			m_y[i] += ny;
		}
	}
}

void Sprite::reduceXY(int nx, int ny)
{
	if (m_imagemode == ImageMode::ALLSAME)
	{
		m_x[0] -= nx;
		m_y[0] -= ny;
	}
	else if (m_imagemode == ImageMode::DEFFIRENT)
	{
		for (int i = 0; i < m_index; i++)
		{
			m_x[i] -= nx;
			m_y[i] -= ny;
		}
	}
}

void Sprite::addPlayDelay(int nDelay, SpriteMode spritemode)//开始时的延迟播放
{
	m_playstarttime = timeGetTime();
	m_playdelay = nDelay;
	m_mode = spritemode;
	m_visible = true;
}

void ImageElement::Delete()
{
	delete image;
	delete image_mask;
}

void ImageElement::Draw(int x,int y)
{
	putimage(x, y, image_mask, SRCAND);
	putimage(x, y, image, SRCPAINT);
}

void Scene::DrawAll()
{
	for (int i = 0; i <m_bkimages.size(); i++)
	{
		putimage(m_bkx+i*(m_bkimages.at(i)->getwidth()), m_bky, m_bkimages.at(i));
	}

	for (int i = 0; i < m_maxz; i++)
	{
		for (int j = 0; j < m_sprites.size(); j++)
		{
			if (m_sprites.at(j)->m_z == i)
			{
				m_sprites.at(j)->DrawAll();
			}
		}
	}
}

void Scene::DeleteAll()
{
	for (int i = 0; i < m_sprites.size(); i++)
		m_sprites.at(i)->DeleteAll();
	for (int i = 0; i < m_bkimages.size(); i++)
		delete m_bkimages.at(i);
	m_bkimages.clear();
	m_sprites.clear();
}

void Scene::Add(Sprite* sprite)
{
	m_sprites.push_back(sprite);
}

void Scene::AddBk(IMAGE* bkimage)
{
	m_bkimages.push_back(bkimage);
}

void Scene::Init(int bkx, int bky, int maxz, SceneClose sceneclose,
	SceneStart scenestart,
	SceneMusic scenemusic,
	CloseSceneMusic closescenemusic,
	SceneAction sceneaction,
	SceneKeyEvent scenekeyevent,
	SceneMouseEvent scenemouseevent,
	SceneCollision sceneCollision)
{
	m_bkx = bkx;
	m_bky = bky;
	m_maxz = maxz;
	m_state = SceneState::START;
	m_sceneclose = sceneclose;
	m_scenestart = scenestart;
	m_scenemusic = scenemusic;
	m_closescenemusic = closescenemusic;
	m_sceneaction = sceneaction;
	m_scenekeyevent = scenekeyevent;
	m_scenemouseevent = scenemouseevent;
	m_scenecollision = sceneCollision;
}

void EasyxGameEngine::AddScene(Scene* scene)
{
	scenes.push_back(scene);
}
void EasyxGameEngine::Paint()//绘制当前场景的全部
{
	scenes.at(m_sceneID)->DrawAll();
}
void EasyxGameEngine::Pause()//暂停整个游戏
{
	//暂定，easyX貌似不好实现
}
void EasyxGameEngine::Active()//从暂停中开始
{
	//暂定
}
void EasyxGameEngine::Init(int delay, int starttime)
{
	m_delay = delay;
	m_starttime = starttime;
	m_sceneID = 0;
}

Scene* EasyxGameEngine::GetCurrentScene()
{
	return scenes.at(m_sceneID);
	//return scenes.at(0);
}

void EasyxGameEngine::InitCurrentScene()
{
	
}

int EasyxGameEngine::GetScenID()
{
	return m_sceneID;
}

int EasyxGameEngine::GetDelay()
{
	return m_delay;
}
int EasyxGameEngine::GetStartTime()
{
	return m_starttime;
}
int EasyxGameEngine::m_delay = 30;//几个静态变量
int EasyxGameEngine::m_starttime = timeGetTime();
int EasyxGameEngine::m_sceneID = 0;

ImageElement* LoadImageElement(char* imagepath, char *image_maskpath,int nHeight,int nWidth,bool resize)//封装加载函数
{
	IMAGE* image = new IMAGE;
	IMAGE* image_mask = new IMAGE;
	ImageElement* imageelement = new ImageElement;
	loadimage(image, imagepath,nWidth,nHeight,resize);
	loadimage(image_mask, image_maskpath, nWidth, nHeight, resize);
	imageelement->image = image;
	imageelement->image_mask = image_mask;
	return imageelement;
}

bool SpriteInSprite(Sprite* sprite1, Sprite* sprite2,int nIndex1,int nIndex2)//判断两个精灵是否相交,要在同Z同显示
{
	if (sprite1->m_z == sprite2->m_z&&sprite1->m_visible&&sprite2->m_visible)
	{
		RECT re1 = { sprite1->m_x[nIndex1],sprite1->m_y[nIndex1],sprite1->m_x[nIndex1] + (sprite1->images.at(nIndex1)->image->getwidth()),
			sprite1->m_y[nIndex1]+(sprite1->images.at(nIndex1)->image->getheight()) };
		RECT re2 = { sprite2->m_x[nIndex2],sprite2->m_y[nIndex2],sprite2->m_x[nIndex2] + (sprite2->images.at(nIndex2)->image->getwidth()),
			sprite2->m_y[nIndex2] + (sprite2->images.at(nIndex2)->image->getheight()) };
		RECT re;
		if (IntersectRect(&re, &re1, &re2))
		{
			return true;
		}
		else
			return false;
	}
	else
		return false;
}