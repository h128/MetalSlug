#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<Windows.h>
#include<stdlib.h>
#include<conio.h>
#include <graphics.h>
#include<vector>
//#include<dsound.h>

enum SpriteMode { ONCE, ALLTIME,PAUSE,ONCEL,ONCEFADE};//动画执行的模式
enum SceneState{START,RUNING,END};//场景的状态
enum ImageMode{ALLSAME,DEFFIRENT};//动画帧的大小不一样的画，必须使用DEFFIRENT，手动设置每帧的坐标

typedef void(*SceneClose)();
typedef void(*SceneStart)();
typedef DWORD(WINAPI*SceneMusic)(void*);//用于创建的线程
typedef void(*CloseSceneMusic)();
typedef void(*SceneAction)();
typedef void(*SceneKeyEvent)();
typedef void(*SceneMouseEvent)(MOUSEMSG);
typedef void(*SceneCollision)();


typedef struct _ImageElement//封装原图和蒙版
{
	IMAGE* image;
	IMAGE* image_mask;
	void Delete();
	void Draw(int x,int y);

}ImageElement;

/*游戏精灵用于管理游戏中的动画或单个图像*/
typedef struct _sprite {
	std::vector<ImageElement*> images;
	bool m_visible;		//控制是否显示动画化
	int m_index;		//动画总帧数
	int m_tempindex;	//当前帧数
	ImageMode m_imagemode;//图片模式,是否相等
	int m_x[50];			//动画坐标
	int m_y[50];
	int m_z;			//z坐标，即绘制的先后顺序
	int m_delay;		//动画帧数
	long m_starttime;	//用于控制帧延迟
	SpriteMode m_mode;
	SpriteMode m_oringmod;//用于记录暂停前的模式
	int m_playdelay;
	long m_playstarttime;
	int m_pausedelay;
	long m_pausedellaystarttime;
	void addPauseDelay(int  nDelay);
	void addPlayDelay(int nDleay, SpriteMode spritemode);
	void addXY(int nX, int nY);//增加x,y的值
	void reduceXY(int nx, int ny);//减少x,y的值
	void Pause(int index);//按照帧的索引暂停
	void Active();
	void DrawAll();		//根据延迟绘制当前帧
	void Add(ImageElement* imageelement);
	void DeleteAll();
	void Init(int x, int y, int z, int delay, int starttime,int index,bool visible=true, SpriteMode nMode=SpriteMode::ALLTIME, ImageMode nImage=ImageMode::ALLSAME);
}Sprite;

typedef struct _scene {
	std::vector<Sprite*> m_sprites;
	std::vector<IMAGE*> m_bkimages;
	int m_bkx;//第一张背景的坐标
	int m_bky;
	int m_maxz;//最大z坐标
	int m_state;//场景当前的状态
	SceneClose m_sceneclose;//场景的处理函数
	SceneStart m_scenestart;
	SceneMusic m_scenemusic;
	CloseSceneMusic m_closescenemusic;
	SceneAction m_sceneaction;
	SceneKeyEvent m_scenekeyevent;
	SceneMouseEvent m_scenemouseevent;
	SceneCollision m_scenecollision;
	void Add(Sprite* sprite);
	void AddBk(IMAGE* bkimage);
	void DrawAll();
	void DeleteAll();
	void Init(int bkx, int bky, int maxz, SceneClose sceneclose,
	SceneStart scenestart,
	SceneMusic scenemusic,
	CloseSceneMusic closescenemusic,
	SceneAction sceneaction,
	SceneKeyEvent scenekeyevent,
	SceneMouseEvent scenemouseevent,
	SceneCollision scenecollision);
}Scene;

typedef struct _EasyxGameEngine
{
	std::vector<Scene*> scenes;
	static int m_delay;//游戏帧频率
	static int m_starttime;
	static int m_sceneID;//当前场景编号
	static int GetDelay();
	static int GetStartTime();
	static int GetScenID();
	void AddScene(Scene* scene);
	void Paint();//绘制当前场景的全部
	void Pause();//暂停整个游戏
	void Active();//从暂停中开始
	void Init(int delay, int starttime);
	Scene* GetCurrentScene();
	void InitCurrentScene();

}EasyxGameEngine;

bool GameStart();//游戏初始化
void GamePaint();//游戏画面绘制
void GameAction();//游戏逻辑
bool GameCollision();//游戏碰撞检测
void GameEnd();//游戏结束
DWORD WINAPI GameMusic(void* nflg);//游戏音乐
void KeyEvent();//按键处理
void MouseEvent(MOUSEMSG msg);//鼠标处理

ImageElement* LoadImageElement(char* imagepath,char *image_maskpath,int nHeight=0,int nWidth=0,bool resize=false);//封装加载函数
bool SpriteInSprite(Sprite* sprite1, Sprite* sprite2,int nIndex1=0,int nIndex2=0);//判断两个精灵是否相交
